#include <gtest/gtest.h>

#include <deep.hpp>
#include <random>

#include "main.hpp"

using namespace cl::sycl;
using namespace dp;

TEST(ScalarTest, Reshape)
{
    auto five = var(5.0);

    auto r = reshape(five, Shape(1, 1, 1));

    try {
        auto buf = r.run(q);

        auto B = buf.get_read_access();
        ASSERT_EQ(B[Index(0, 0, 0)], 5.0);
    } catch (cl::sycl::exception const& e) {
        std::cerr << e.what() << "\n";
        throw;
    }
}

TEST(ScalarTest, Reshape2)
{
    auto five = var(Shape{ 1, 1, 1 }, { 5.0 });

    auto r = reshape(five, Shape{});
    auto buf = r.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 5.0);
}

TEST(ScalarTest, Update)
{
    auto A = var(5.0);
    auto B = var(6.0);

    {
        auto C = A.get_read_access();
        ASSERT_EQ(C[Index{}], 5.0);
    }

    auto tok = update(A, B);
    tok.run(q);

    {
        auto C = A.get_read_access();
        ASSERT_EQ(C[Index{}], 6.0);
    }
}

TEST(ScalarTest, Addition)
{
    auto five = var(5.0);
    auto six = var(6.0);

    auto add = five + six;
    auto buf = add.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 11.0);
}

TEST(ScalarTest, Multiplication)
{
    auto five = var(5.0);
    auto six = var(6.0);

    auto add = five * six;
    auto buf = add.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 30.0);
}

TEST(ScalarTest, Division)
{
    auto five = var(5.0);
    auto six = var(20.0);

    auto div = five / six;
    auto buf = div.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 0.25);
}

TEST(ScalarTest, AdditionWithDuplicates)
{
    auto five = var(5.0);
    auto six = var(6.0);

    auto add = five + six + six + six + five;
    auto buf = add.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 28.0);
}

TEST(VectorTest, Addition)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto add = A + B;
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], 11.0);
    ASSERT_EQ(C[Index(1)], 3.0);
}

TEST(VectorTest, ElemwiseMax)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto add = elem_fmax(A, B);
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], 6.0);
    ASSERT_EQ(C[Index(1)], 2.0);
}

TEST(VectorTest, ElemwisePow)
{
    auto A = var(Shape(2), { 5.0, 3.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto add = elem_pow(A, B);
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], 15625.0);
    ASSERT_EQ(C[Index(1)], 9.0);
}

TEST(VectorTest, ElemwiseDivision)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 20.0, 2.0 });

    auto div = elem_div(A, B);
    auto buf = div.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], 0.25);
    ASSERT_EQ(C[Index(1)], 0.5);
}

TEST(VectorTest, Reshape)
{
    auto A = var(Shape(4), { 5.0, 1.0, 3.0, 4.0 });

    auto r = reshape(A, Shape(1, 2, 2));
    auto buf = r.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0, 0)], 5.0);
    ASSERT_EQ(C[Index(0, 0, 1)], 1.0);
    ASSERT_EQ(C[Index(0, 1, 0)], 3.0);
    ASSERT_EQ(C[Index(0, 1, 1)], 4.0);
}

TEST(VectorTest, Subtraction)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto sub = A - B;
    auto buf = sub.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], -1.0);
    ASSERT_EQ(C[Index(1)], -1.0);
}

TEST(VectorTest, Negation)
{
    auto A = var(Shape(3), { 5.0, 1.0, 2.0 });

    auto neg = -A;
    auto buf = neg.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], -5.0);
    ASSERT_EQ(C[Index(1)], -1.0);
    ASSERT_EQ(C[Index(2)], -2.0);
}

TEST(VectorTest, Arange)
{
    auto A = arange<double>(10);

    auto buf = A.run(q);

    auto C = buf.get_read_access();

    ASSERT_EQ(C.shape(), Shape{ 10 });

    ASSERT_EQ(C[Index(0)], 0.0);
    ASSERT_EQ(C[Index(1)], 1.0);
    ASSERT_EQ(C[Index(2)], 2.0);
    ASSERT_EQ(C[Index(3)], 3.0);
    ASSERT_EQ(C[Index(4)], 4.0);
    ASSERT_EQ(C[Index(5)], 5.0);
    ASSERT_EQ(C[Index(6)], 6.0);
    ASSERT_EQ(C[Index(7)], 7.0);
    ASSERT_EQ(C[Index(8)], 8.0);
    ASSERT_EQ(C[Index(9)], 9.0);
}

TEST(VectorTest, EnsureShapeFail)
{
    auto A = var(Shape(3), { 5.0, 1.0, 2.0 });

    auto res = ensure_shape<100>(A);

    ASSERT_DEBUG_DEATH(
        auto buf = res.run(q);
        , "Shapes do not match");
}

TEST(VectorTest, Sum)
{
    auto A = var(Shape(3), { 5.0, 1.0, 4.0 });

    auto s = sum<0>(A);
    auto buf = s.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index{}], 10.0);
}

TEST(VectorTest, Mean)
{
    auto A = var(Shape(3), { 5.0, 1.0, 4.0 });

    auto s = mean<0>(A);
    auto buf = s.run(q);

    auto C = buf.get_read_access();
    ASSERT_DOUBLE_EQ(C[Index{}], 10.0 / 3.0);
}

TEST(VectorTest, SumKeepDim)
{
    auto A = var(Shape(3), { 5.0, 1.0, 4.0 });

    auto s = sum_keep_dim<0>(A);
    auto buf = s.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0)], 10.0);
}

TEST(VectorTest, MeanKeepDim)
{
    auto A = var(Shape(3), { 5.0, 1.0, 4.0 });

    auto s = mean_keep_dim<0>(A);
    auto buf = s.run(q);

    auto C = buf.get_read_access();
    ASSERT_DOUBLE_EQ(C[Index(0)], 10.0 / 3.0);
}

TEST(MatrixTest, Addition)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });
    auto B = var(Shape(2, 2), { 6.0, 2.0, -2.0, -4.0 });

    auto add = A + B;
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 11.0);
    ASSERT_EQ(C[Index(0, 1)], 3.0);
    ASSERT_EQ(C[Index(1, 0)], -1.0);
    ASSERT_EQ(C[Index(1, 1)], -1.0);
}

TEST(MatrixTest, Broadcasting)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });
    auto B = ensure_shape<1, 2>(var(Shape(1, 2), { 6.0, 2.0 }));

    auto add = A + B;
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 11.0);
    ASSERT_EQ(C[Index(0, 1)], 3.0);
    ASSERT_EQ(C[Index(1, 0)], 7.0);
    ASSERT_EQ(C[Index(1, 1)], 5.0);
}

TEST(MatrixTest, BroadcastingDifferentRanks)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto add = A + B;
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 11.0);
    ASSERT_EQ(C[Index(0, 1)], 3.0);
    ASSERT_EQ(C[Index(1, 0)], 7.0);
    ASSERT_EQ(C[Index(1, 1)], 5.0);
}

TEST(MatrixTest, BroadcastingDifferentRanks2)
{
    auto A = var(Shape(2), { 6.0, 2.0 });
    auto B = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });

    auto add = A + B;
    auto buf = add.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 11.0);
    ASSERT_EQ(C[Index(0, 1)], 3.0);
    ASSERT_EQ(C[Index(1, 0)], 7.0);
    ASSERT_EQ(C[Index(1, 1)], 5.0);
}

TEST(MatrixTest, BroadcastingNotMatching)
{
    auto A = var(Shape(2), { 6.0, 2.0 });
    auto B = var(Shape(1, 4), { 5.0, 1.0, 1.0, 3.0 });

    auto add = A + B;

    ASSERT_DEBUG_DEATH(
        auto buf = add.run(q);
        , "Shapes are not compatible");
}

TEST(MatrixTest, ElemwiseMul)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });
    auto B = ensure_shape<1, UnknownDim>(var(Shape(1, 2), { 6.0, 2.0 }));

    auto mul = elem_mul(A, B);
    auto buf = mul.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 30.0);
    ASSERT_EQ(C[Index(0, 1)], 2.0);
    ASSERT_EQ(C[Index(1, 0)], 6.0);
    ASSERT_EQ(C[Index(1, 1)], 6.0);
}

TEST(MatrixTest, Update)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });
    auto B = var(Shape(2, 2), { 6.0, 2.0, 2.0, 4.0 });

    {
        auto C = A.get_read_access();
        ASSERT_EQ(C[Index(0, 0)], 5.0);
        ASSERT_EQ(C[Index(0, 1)], 1.0);
        ASSERT_EQ(C[Index(1, 0)], 1.0);
        ASSERT_EQ(C[Index(1, 1)], 3.0);
    }

    auto tok = update(A, B);
    tok.run(q);

    {
        auto C = A.get_read_access();
        ASSERT_EQ(C[Index(0, 0)], 6.0);
        ASSERT_EQ(C[Index(0, 1)], 2.0);
        ASSERT_EQ(C[Index(1, 0)], 2.0);
        ASSERT_EQ(C[Index(1, 1)], 4.0);
    }
}

TEST(MatrixTest, Select)
{
    auto A = var<bool>(Shape(2, 2), { true, false, false, true });
    auto B = ensure_shape<1, UnknownDim>(var(Shape(1, 2), { 6.0, 2.0 }));
    auto C = ensure_shape<UnknownDim, 1>(var(Shape(2, 1), { 3.0, 5.0 }));

    auto mul = select(A, B, C);
    auto buf = mul.run(q);

    auto D = buf.get_read_access();
    ASSERT_EQ(D[Index(0, 0)], 6.0);
    ASSERT_EQ(D[Index(0, 1)], 3.0);
    ASSERT_EQ(D[Index(1, 0)], 5.0);
    ASSERT_EQ(D[Index(1, 1)], 2.0);
}

TEST(MatrixTest, ScalarMul)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });

    auto mul = A * var(2.0);
    auto buf = mul.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 10.0);
    ASSERT_EQ(C[Index(0, 1)], 2.0);
    ASSERT_EQ(C[Index(1, 0)], 2.0);
    ASSERT_EQ(C[Index(1, 1)], 6.0);
}

TEST(MatrixTest, ScalarMul2)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });

    auto mul = var(2.0) * A;
    auto buf = mul.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 10.0);
    ASSERT_EQ(C[Index(0, 1)], 2.0);
    ASSERT_EQ(C[Index(1, 0)], 2.0);
    ASSERT_EQ(C[Index(1, 1)], 6.0);
}

TEST(MatrixTest, DivByScalar)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 1.0, 3.0 });

    auto div = A / var(2.0);
    auto buf = div.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 2.5);
    ASSERT_EQ(C[Index(0, 1)], 0.5);
    ASSERT_EQ(C[Index(1, 0)], 0.5);
    ASSERT_EQ(C[Index(1, 1)], 1.5);
}

TEST(MatrixTest, Pad)
{
    std::vector<double> a{ 5.0, 1.0, 1.0, 3.0 };
    auto A = var(Shape(2, 2), a.begin());

    auto padded = pad(A, Shape(1, 2), Shape(3, 4));
    auto buf = padded.run(q);

    ASSERT_EQ(buf.shape(), Shape(6, 8));

    auto C = buf.get_read_access();

    for (std::size_t i = 0; i < 6; ++i) {
        for (std::size_t j = 0; j < 8; ++j) {
            if (1 <= i && i < 3 && 2 <= j && j < 4) {
                ASSERT_EQ(C[Index(i, j)], a[(i - 1) * 2 + (j - 2)]);
            } else {
                ASSERT_EQ(C[Index(i, j)], 0.0);
            }
        }
    }
}

TEST(MatrixTest, Transpose)
{
    auto A = var(Shape(2, 2), { 5.0, 1.0, 2.0, 3.0 });

    auto trans = transpose<0, 1>(A);
    auto buf = trans.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 5.0);
    ASSERT_EQ(C[Index(0, 1)], 2.0);
    ASSERT_EQ(C[Index(1, 0)], 1.0);
    ASSERT_EQ(C[Index(1, 1)], 3.0);
}

TEST(MatrixTest, Multiplication)
{
    auto A = var(Shape(3, 2), { 5.0, 1.0, 1.0, 3.0, 2.0, 5.0 });
    auto B = var(Shape(2, 3), { 6.0, 2.0, -2.0, -4.0, 7.0, 11.0 });

    auto mul = A * B;

    try {
        auto buf = mul.run(q);

        auto C = buf.get_read_access();
        ASSERT_EQ(C[Index(0, 0)], 26.0);
        ASSERT_EQ(C[Index(0, 1)], 17.0);
        ASSERT_EQ(C[Index(0, 2)], 1.0);
        ASSERT_EQ(C[Index(1, 0)], -6.0);
        ASSERT_EQ(C[Index(1, 1)], 23.0);
        ASSERT_EQ(C[Index(1, 2)], 31.0);
        ASSERT_EQ(C[Index(2, 0)], -8.0);
        ASSERT_EQ(C[Index(2, 1)], 39.0);
        ASSERT_EQ(C[Index(2, 2)], 51.0);
    } catch (cl::sycl::exception const& e) {
        std::cerr << e.what() << "\n";
        throw;
    }
}

TEST(MatrixTest, MultiplicationFail)
{
    auto A = var(Shape(2, 3), { 5.0, 1.0, 1.0, 3.0, 2.0, 5.0 });
    auto B = var(Shape(2, 3), { 6.0, 2.0, -2.0, -4.0, 7.0, 11.0 });

    auto mul = A * B;

    ASSERT_DEBUG_DEATH(
        auto buf = mul.run(q);
        , "Matrix inner dims must be the same");
}

TEST(MatrixTest, MatVecMult)
{
    auto A = var(Shape(3), { 5.0, 1.0, 1.0 });
    auto B = var(Shape(3, 3), { 6.0, 2.0, -2.0, -4.0, 7.0, 11.0, 5.0, 1.0, 1.0 });

    auto mul = add_dim<0>(A) * B;
    auto buf = mul.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 31.0);
    ASSERT_EQ(C[Index(0, 1)], 18.0);
    ASSERT_EQ(C[Index(0, 2)], 2.0);
}

TEST(MatrixTest, MatVecMult2)
{
    auto A = var(Shape(3), { 5.0, 1.0, 1.0 });
    auto B = var(Shape(3, 3), { 6.0, 2.0, -2.0, -4.0, 7.0, 11.0, 5.0, 1.0, 1.0 });

    auto mul = B * add_dim<1>(A);
    auto buf = mul.run(q);

    auto C = buf.get_read_access();
    ASSERT_EQ(C[Index(0, 0)], 30.0);
    ASSERT_EQ(C[Index(1, 0)], -2.0);
    ASSERT_EQ(C[Index(2, 0)], 27.0);
}

TEST(TensorTest, Tensor3D)
{
    std::vector<double> a(27);
    std::iota(a.begin(), a.end(), 1.0);

    auto A = var(Shape(3, 3, 3), a.begin());

    auto buf = A.run(q);

    auto C = buf.get_read_access();
    double cnt = 0.0;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                cnt++;
                ASSERT_EQ(C[Index(i, j, k)], cnt);
            }
        }
    }
}

TEST(TensorTest, AddDim)
{
    std::vector<double> a(27);
    std::iota(a.begin(), a.end(), 1.0);

    auto A = add_dim<1>(var(Shape(3, 3, 3), a.begin()));

    ASSERT_EQ(decltype(A)::SShape::to_shape(),
        Shape(UnknownDim, 1, UnknownDim, UnknownDim));

    auto buf = A.run(q);

    auto C = buf.get_read_access();
    double cnt = 0.0;
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                cnt++;
                ASSERT_EQ(C[Index(i, 0, j, k)], cnt);
            }
        }
    }
}

TEST(TensorTest, Tensor4D)
{
    auto shape = Shape(3, 4, 5, 6);
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 1.0);

    auto A = var(shape, a.begin());

    auto buf = A.run(q);

    auto C = buf.get_read_access();
    double cnt = 0.0;
    for (std::size_t i = 0; i < shape[0]; ++i) {
        for (std::size_t j = 0; j < shape[1]; ++j) {
            for (std::size_t k = 0; k < shape[2]; ++k) {
                for (std::size_t l = 0; l < shape[3]; ++l) {
                    cnt++;
                    ASSERT_EQ(C[Index(i, j, k, l)], cnt);
                }
            }
        }
    }
}

TEST(TensorTest, Tensor5D)
{
    auto shape = Shape(2, 3, 4, 5, 6);

    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 1.0);

    auto A = var(shape, a.begin());

    auto buf = A.run(q);

    auto C = buf.get_read_access();
    double cnt = 0.0;
    for (std::size_t i = 0; i < shape[0]; ++i) {
        for (std::size_t j = 0; j < shape[1]; ++j) {
            for (std::size_t k = 0; k < shape[2]; ++k) {
                for (std::size_t l = 0; l < shape[3]; ++l) {
                    for (std::size_t m = 0; m < shape[4]; ++m) {
                        cnt++;
                        ASSERT_EQ(C[Index(i, j, k, l, m)], cnt);
                    }
                }
            }
        }
    }
}

TEST(TensorTest, TileTensor)
{
    std::vector<double> a(9);
    std::iota(a.begin(), a.end(), 0.0);
    auto A = var(Shape(3, 1, 3), a.begin());

    auto B = tile<1>(A, 3);

    auto buf = B.run(q);

    auto C = buf.get_read_access();

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                ASSERT_EQ(C[Index(i, j, k)], i * 3 + k);
            }
        }
    }
}

TEST(TensorTest, OnesLikeTensor)
{
    std::vector<double> a(27);
    std::iota(a.begin(), a.end(), 0.0);
    auto A = var(Shape(3, 3, 3), a.begin());
    auto B = ones_like(A);

    auto buf = B.run(q);

    auto C = buf.get_read_access();

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                ASSERT_EQ(C[Index(i, j, k)], 1.0);
            }
        }
    }
}

TEST(TensorTest, OnesTensor)
{
    auto A = ones<double>(Shape(3, 3, 3));

    auto buf = A.run(q);

    auto C = buf.get_read_access();

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 3; ++k) {
                ASSERT_EQ(C[Index(i, j, k)], 1.0);
            }
        }
    }
}

TEST(TensorTest, ShapeTensor)
{
    auto A = ones<double>(Shape(3, 4, 5, 6, 7));

    auto shape = to_tensor(shape_of(A));

    auto buf = shape.run(q);

    auto C = buf.get_read_access();

    ASSERT_EQ(C[Index{ 0 }], 3);
    ASSERT_EQ(C[Index{ 1 }], 4);
    ASSERT_EQ(C[Index{ 2 }], 5);
    ASSERT_EQ(C[Index{ 3 }], 6);
    ASSERT_EQ(C[Index{ 4 }], 7);
}

TEST(TensorTest, ShapeTensor2)
{
    auto shape = to_tensor(lazy_shape(Shape{ 1 }));

    auto buf = shape.run(q);

    auto C = buf.get_read_access();

    ASSERT_EQ(C[Index{ 0 }], 1);
}

TEST(TensorTest, Index)
{
    auto shape = Shape{ 5, 6, 7, 9, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto B = A.index(Range{ Last, 0, -1 }, Range{ 1, 1 }, 4, Range{ 0, Last, 2 });

    auto buf_A = A.run(q);
    auto buf_B = B.run(q);

    auto Aacc = buf_A.get_read_access();
    auto Bacc = buf_B.get_read_access();

    ASSERT_EQ(Bacc.shape(), Shape(5, 1, 5, 9));
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 0, 0, 0)], Aacc[Index(4, 1, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(4, 0, 3, 8)], Aacc[Index(0, 1, 4, 6, 8)]);
}

TEST(TensorTest, Index2)
{
    auto shape = Shape{ 9, 8, 5, 6, 7 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto B = A.index(Range{ Last, 0, -2 }, 3, 4, Range{ Last, 0, -3 });

    auto buf_A = A.run(q);
    auto buf_B = B.run(q);

    auto Aacc = buf_A.get_read_access();
    auto Bacc = buf_B.get_read_access();

    ASSERT_EQ(Bacc.shape(), Shape(5, 2, 7));
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 0, 0)], Aacc[Index(8, 3, 4, 5, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(4, 1, 6)], Aacc[Index(0, 3, 4, 2, 6)]);
}

TEST(TensorTest, IndexWithArray)
{
    auto shape = Shape{ 5, 6, 7, 9, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto I = var(Shape{ 7 }, { 5, 3, 1, 2, 5, 0, 4 });
    auto B = A.index(Range{ Last, 0, -1 }, I, 4, Range{ 0, Last, 2 });

    auto buf_A = A.run(q);
    Queue qu{ host_selector{} };
    auto buf_B = B.run(qu);

    auto Aacc = buf_A.get_read_access();
    auto Bacc = buf_B.get_read_access();

    ASSERT_EQ(Bacc.shape(), Shape(7, 5, 5, 9));
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 0, 0, 0)], Aacc[Index(4, 5, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 4, 3, 8)], Aacc[Index(0, 5, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(1, 0, 0, 0)], Aacc[Index(4, 3, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(1, 4, 3, 8)], Aacc[Index(0, 3, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(2, 0, 0, 0)], Aacc[Index(4, 1, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(2, 4, 3, 8)], Aacc[Index(0, 1, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(3, 0, 0, 0)], Aacc[Index(4, 2, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(3, 4, 3, 8)], Aacc[Index(0, 2, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(4, 0, 0, 0)], Aacc[Index(4, 5, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(4, 4, 3, 8)], Aacc[Index(0, 5, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(5, 0, 0, 0)], Aacc[Index(4, 0, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(5, 4, 3, 8)], Aacc[Index(0, 0, 4, 6, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(6, 0, 0, 0)], Aacc[Index(4, 4, 4, 0, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(6, 4, 3, 8)], Aacc[Index(0, 4, 4, 6, 8)]);
}

TEST(TensorTest, IndexWithArray2)
{
    auto shape = Shape{ 5, 6, 7, 9, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto I = var(Shape{ 7 }, { 5, 3, 1, 2, 5, 0, 4 });
    auto I2 = var(Shape{ 7 }, { 1, 3, 2, 1, 3, 2, 2 });
    auto B = A.index(Range{ Last, 0, -1 }, I, 4, I2);

    auto buf_A = A.run(q);
    auto buf_B = B.run(q);

    auto Aacc = buf_A.get_read_access();
    auto Bacc = buf_B.get_read_access();

    ASSERT_EQ(Bacc.shape(), Shape(7, 5, 9));
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 0, 0)], Aacc[Index(4, 5, 4, 1, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(0, 4, 8)], Aacc[Index(0, 5, 4, 1, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(1, 0, 0)], Aacc[Index(4, 3, 4, 3, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(1, 4, 8)], Aacc[Index(0, 3, 4, 3, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(2, 0, 0)], Aacc[Index(4, 1, 4, 2, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(2, 4, 8)], Aacc[Index(0, 1, 4, 2, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(3, 0, 0)], Aacc[Index(4, 2, 4, 1, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(3, 4, 8)], Aacc[Index(0, 2, 4, 1, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(4, 0, 0)], Aacc[Index(4, 5, 4, 3, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(4, 4, 8)], Aacc[Index(0, 5, 4, 3, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(5, 0, 0)], Aacc[Index(4, 0, 4, 2, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(5, 4, 8)], Aacc[Index(0, 0, 4, 2, 8)]);

    ASSERT_DOUBLE_EQ(Bacc[Index(6, 0, 0)], Aacc[Index(4, 4, 4, 2, 0)]);
    ASSERT_DOUBLE_EQ(Bacc[Index(6, 4, 8)], Aacc[Index(0, 4, 4, 2, 8)]);
}

TEST(TensorTest, Scatter)
{
    std::vector<std::size_t> ind = { 3, 2, 0, 4 };
    auto indices = var(Shape{ 4 }, ind.begin());

    std::vector<double> upd = {
        1, 2, 3, //
        4, 5, 6, //
        7, 8, 9, //
        10, 11, 12, //
    };
    auto updates = var(Shape{ 4, 3 }, upd.begin());

    auto expr = scatter(indices, updates, Shape{ 10, 3 });
    auto result = expr.run(q);
    auto buf = result.get_read_access();

    ASSERT_DOUBLE_EQ(buf[Index(0, 0)], 7);
    ASSERT_DOUBLE_EQ(buf[Index(0, 1)], 8);
    ASSERT_DOUBLE_EQ(buf[Index(0, 2)], 9);
    ASSERT_DOUBLE_EQ(buf[Index(1, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(1, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(1, 2)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(2, 0)], 4);
    ASSERT_DOUBLE_EQ(buf[Index(2, 1)], 5);
    ASSERT_DOUBLE_EQ(buf[Index(2, 2)], 6);
    ASSERT_DOUBLE_EQ(buf[Index(3, 0)], 1);
    ASSERT_DOUBLE_EQ(buf[Index(3, 1)], 2);
    ASSERT_DOUBLE_EQ(buf[Index(3, 2)], 3);
    ASSERT_DOUBLE_EQ(buf[Index(4, 0)], 10);
    ASSERT_DOUBLE_EQ(buf[Index(4, 1)], 11);
    ASSERT_DOUBLE_EQ(buf[Index(4, 2)], 12);
    ASSERT_DOUBLE_EQ(buf[Index(5, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(5, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(5, 2)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(6, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(6, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(6, 2)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(7, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(7, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(7, 2)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(8, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(8, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(8, 2)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(9, 0)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(9, 1)], 0);
    ASSERT_DOUBLE_EQ(buf[Index(9, 2)], 0);
}
