#include <gtest/gtest.h>

#include <deep.hpp>
#include <random>

#include "main.hpp"

using namespace cl::sycl;
using namespace dp;

TEST(BigInputsTest, MatrixMultiplication)
{
    using namespace std::chrono;

    for (std::size_t N = 1; N <= 256; N *= 2) {
        std::cout << "N = " << N << std::endl;

        std::vector<double> a(N * N);
        std::vector<double> b(N * N);

        std::vector<double> c(N * N);

        std::mt19937 gen(std::random_device{}());
        std::uniform_real_distribution<double> dis(-10.0, 10.0);
        std::generate(a.begin(), a.end(), [&] { return dis(gen); });
        std::generate(b.begin(), b.end(), [&] { return dis(gen); });

        auto cpu_start = system_clock::now();
        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                for (std::size_t k = 0; k < N; ++k) {
                    c[i * N + j] += a[i * N + k] * b[k * N + j];
                }
            }
        }
        auto cputime = duration<double>(system_clock::now() - cpu_start);
        std::cout << "CPU elapsed : " << cputime.count() << "s\n";

        auto sycl_start = system_clock::now();
        auto A = var(Shape(N, N), a.begin());
        auto B = var(Shape(N, N), b.begin());

        auto mul = A * B;
        auto buf = mul.run(q);

        auto C = buf.get_read_access();
        auto sycltime = duration<double>(system_clock::now() - sycl_start);
        std::cout << "SYCL elapsed : " << sycltime.count() << "s\n";
        std::cout << cputime / sycltime << "x speedup" << std::endl;

        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                ASSERT_NEAR(C[Index(i, j)], c[i * N + j], 0.01);
            }
        }
    }
}
