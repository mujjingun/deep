#include <gtest/gtest.h>

#include "grad_check.hpp"
#include "main.hpp"

using namespace cl::sycl;
using namespace dp;

TEST(GradientTest, Simple)
{
    auto five = set_id<NumericId<0>>(var(5.0));

    auto g = grad(five, five);
    auto buf = g.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 1.0);
}

TEST(GradientTest, ScalarAddition)
{
    auto five = set_id<NumericId<0>>(var(5.0));
    auto six = var(6.0);

    auto g = grad(five + six, five);
    auto buf = g.run(q);

    auto B = buf.get_read_access();
    ASSERT_EQ(B[Index{}], 1.0);
}

TEST(GradientTest, VectorAddition)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto func = [&](auto A) {
        return sum<0>(elem_mul(A, A) + B);
    };

    check_gradient(func, A);
}

TEST(GradientTest, VectorElemwiseMul)
{
    auto A = var(Shape(2), { 5.0, -1.0, -2.0, 2.0 });
    auto B = var(Shape(2), { -6.0, 2.0, -3.0, 8.0 });

    auto func = [&](auto A) {
        return sum<0>(elem_mul(elem_mul(A, A), B));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Log)
{
    auto A = var(Shape(2), { 5.0, 1.0 });

    auto func = [&](auto A) {
        auto res = elem_log(elem_mul(A, A));
        return sum<0>(elem_mul(res, res));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Exp)
{
    auto A = var(Shape(2), { 0.03, 1.0 });

    auto func = [&](auto A) {
        auto res = elem_exp(elem_mul(A, A));
        return sum<0>(elem_mul(res, res));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Divide)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto func = [&](auto A) {
        auto res = elem_div(A, B);
        return sum<0>(elem_mul(res, res));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Divide2)
{
    auto A = var(Shape(2), { 5.0, 1.0 });
    auto B = var(Shape(2), { 6.0, 2.0 });

    auto func = [&](auto A) {
        auto res = elem_div(B, A);
        return sum<0>(elem_mul(res, res));
    };

    check_gradient(func, A);
}

TEST(GradientTest, ReduceMax)
{
    auto A = var(Shape(5), { 5.0, 1.0, 3.0, 6.0, -1.0 });

    auto func = [&](auto A) {
        auto res = max<0>(elem_mul(A, A));
        return res;
    };

    check_gradient(func, A);
}

TEST(GradientTest, ReduceMax2)
{
    auto A = var(Shape(5), { -5.0, -1.0, -3.0, -6.0, -10.0 });

    auto func = [&](auto A) {
        auto res = max<0>(A);
        return res;
    };

    check_gradient(func, A);
}

TEST(GradientTest, ReduceMin)
{
    auto A = var(Shape(5), { 5.0, 1.0, 3.0, 6.0, -1.0 });

    auto func = [&](auto A) {
        auto res = min<0>(A);
        return res;
    };

    check_gradient(func, A);
}

TEST(GradientTest, ReduceMin2)
{
    auto A = var(Shape(5), { 5.0, 10.0, 3.0, 6.0, 1.0 });

    auto func = [&](auto A) {
        auto res = min<0>(elem_mul(A, A));
        return res;
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAddition)
{
    auto shape = Shape(2, 3);

    std::vector<double> a(shape.size());
    std::vector<double> b(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());
    auto B = var(shape, b.begin());

    auto func = [&](auto A) {
        auto add = elem_mul(A, A) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixElemwiseMax)
{
    auto shape = Shape(3, 3);

    std::vector<double> a(shape.size());
    std::vector<double> b(shape.size());
    std::vector<double> c(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });
    std::generate(c.begin(), c.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());
    auto B = var(shape, b.begin());
    auto C = var(shape, c.begin());

    auto func1 = [&](auto A) {
        auto r = elem_fmax(elem_mul(A, A), B) * C;
        return sum<0>(sum<0>(r));
    };

    check_gradient(func1, A);

    auto func2 = [&](auto B) {
        auto r = elem_fmax(A, elem_mul(B, B)) * C;
        return sum<0>(sum<0>(r));
    };

    check_gradient(func2, B);
}

TEST(GradientTest, MatrixAdditionWithBroadcasting)
{
    auto shape_a = Shape(4, 10);
    auto shape_b = Shape(1, 10);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = ensure_shape<1, UnknownDim>(var(shape_b, b.begin()));

    auto func = [&](auto A) {
        auto add = elem_mul(A, A) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithBroadcasting2)
{
    auto shape_a = Shape(1, 10);
    auto shape_b = Shape(4, 10);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = ensure_shape<1, UnknownDim>(var(shape_a, a.begin()));
    auto B = var(shape_b, b.begin());

    auto func = [&](auto A) {
        auto add = elem_mul(A, A) + B;
        return sum_all(add);
    };

    check_gradient(func, A);
}

TEST(GradientTest, ReshapeLike)
{
    auto shape_a = Shape(4, 25);
    auto shape_b = Shape(10, 10);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = var(shape_b, b.begin());

    auto func = [&](auto A) {
        auto add = reshape_like(elem_mul(A, A), B) * B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithBroadcastingAndSumKeepDim)
{
    auto shape_a = Shape(20, 10);
    auto shape_b = Shape(4, 10);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = var(shape_b, b.begin());

    auto func = [&](auto A) {
        auto add = sum_keep_dim<0>(elem_mul(A, A)) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithSumLike)
{
    auto shape_a = Shape(20, 10);
    auto shape_b = Shape(1, 10);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = ensure_shape<1, UnknownDim>(var(shape_b, b.begin()));

    auto func = [&](auto A) {
        auto add = sum_like(elem_mul(A, A), B) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithSumLike2)
{
    auto shape_a = Shape(20, 10);
    auto shape_b = Shape(20, 1);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = ensure_shape<UnknownDim, 1>(var(shape_b, b.begin()));

    auto func = [&](auto A) {
        auto add = sum_like(elem_mul(A, A), B) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithSumLike3)
{
    auto shape_a = Shape(20, 10);
    auto shape_b = Shape(1, 1);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = ensure_shape<1, 1>(var(shape_b, b.begin()));

    auto func = [&](auto A) {
        auto add = sum_like(elem_mul(A, A), B) + B;
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixAdditionWithBroadcastingAndSumKeepDim2)
{
    auto shape_a = Shape(20, 10);
    auto shape_b = Shape(20, 30);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = var(shape_b, b.begin());

    auto func = [&](auto A) {
        auto add = elem_mul(A, A) + sum_keep_dim<1>(B);
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Transpose)
{
    auto shape = Shape(5, 10);

    std::vector<double> a(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());

    auto func = [&](auto A) {
        auto add = transpose<0, 1>(elem_mul(A, A));
        return sum<0>(sum<0>(add));
    };

    check_gradient(func, A);
}

TEST(GradientTest, Reshape)
{
    auto shape = Shape(8, 9);

    std::vector<double> a(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());

    auto func = [&](auto A) {
        auto res = reshape(elem_mul(A, A), Shape(3, 4, 3, 2));
        return sum_all(res);
    };

    check_gradient(func, A);
}

TEST(GradientTest, MatrixMultiplication)
{
    auto shape_a = Shape(4, 10);
    auto shape_b = Shape(10, 5);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = var(shape_b, b.begin());

    auto func = [&](auto A) {
        auto mul = elem_mul(A, A) * B;
        return sum<0>(sum<0>(mul));
    };

    check_gradient(func, A);
}

TEST(GradientTest, AddDimension)
{
    auto shape = Shape(5, 10);

    std::vector<double> a(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());

    auto func = [&](auto A) {
        auto add = add_dim<4>(add_dim<2>(add_dim<0>(elem_mul(A, A))));
        return sum_all(add);
    };

    check_gradient(func, A);
}

TEST(GradientTest, Index)
{
    auto shape = Shape{ 5, 6, 2, 8, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());

    auto func = [&](auto A) {
        auto A2 = elem_mul(A, A);
        auto B = A2.index(Range{}, Range{ 3, 5 }, 1, Range{ 0, Last, 2 });
        return sum_all(B);
    };

    check_gradient<100>(func, A, q);
}

TEST(GradientTest, Index2)
{
    auto shape = Shape{ 5, 6, 2, 8, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());

    auto func = [&](auto A) {
        auto A2 = elem_mul(A, A);
        auto B = A2.index(Range{ Last, 0, -1 }, Range{ 3, 5 }, 1, Range{ Last, 0, -2 });
        return sum_all(B);
    };

    check_gradient<100>(func, A, q);
}

TEST(GradientTest, IndexWithArray)
{
    auto shape = Shape{ 5, 6, 2, 8, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto I = var(Shape(7), { 0, 1, 3, 0, 2, 5, 2 });

    auto func = [&](auto A) {
        auto A2 = elem_mul(A, A);
        auto B = A2.index(Range{ Last, 0, -1 }, I, 1, Range{ Last, 0, -2 });
        return sum_all(B);
    };

    check_gradient<100>(func, A, q);
}

TEST(GradientTest, IndexWithArray2)
{
    auto shape = Shape{ 5, 6, 2, 8, 9 };
    std::vector<double> a(shape.size());
    std::iota(a.begin(), a.end(), 0.0);

    auto A = var(shape, a.begin());
    auto I = var(Shape(7), { 0, 1, 3, 0, 2, 5, 2 });
    auto J = var(Shape(7), { 0, 0, 1, 0, 1, 1, 0 });

    auto func = [&](auto A) {
        auto A2 = elem_mul(A, A);
        auto B = A2.index(Range{ Last, 0, -1 }, I, J, Range{ Last, 0, -2 });
        return sum_all(B);
    };

    check_gradient<100>(func, A, q);
}

TEST(GradientTest, ForwardAndBackward)
{
    std::vector<double> a{ 5.0, 1.0, 1.0, 3.0, 2.0, 5.0 };
    std::vector<double> b{ 6.0, 2.0, -2.0, -4.0, 7.0, 11.0 };

    auto A = set_id<NumericId<0>>(var(Shape(3, 2), a.begin()));
    auto B = var(Shape(2, 3), b.begin());

    auto mul = A * B;
    auto s = sum_all(mul);
    auto g = grad(s, A);
    auto tup = dp::make_tuple(s, g);

    try {
        auto [S, G] = tup.run(q);

        ASSERT_EQ(S.get_read_access()[Index{}], 174.0);

        auto gacc = G.get_read_access();
        ASSERT_EQ((gacc[Index{ 0, 0 }]), 6.0);
        ASSERT_EQ((gacc[Index{ 0, 1 }]), 14.0);
        ASSERT_EQ((gacc[Index{ 1, 0 }]), 6.0);
        ASSERT_EQ((gacc[Index{ 1, 1 }]), 14.0);
        ASSERT_EQ((gacc[Index{ 2, 0 }]), 6.0);
        ASSERT_EQ((gacc[Index{ 2, 1 }]), 14.0);

    } catch (cl::sycl::exception const& e) {
        std::cerr << e.what() << "\n";
        throw;
    }
}
TEST(GradientTest, MatMulBias)
{
    auto shape_a = Shape(4, 10);
    auto shape_b = Shape(10, 5);
    auto shape_c = Shape(5);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());
    std::vector<double> c(shape_c.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });
    std::generate(c.begin(), c.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = var(shape_b, b.begin());
    auto C = var(shape_c, c.begin());

    auto func1 = [&](auto C) {
        auto R = elem_fmax(A * B + C, var(0.0));
        return sum<0>(sum<0>(R));
    };

    check_gradient(func1, C);

    auto func2 = [&](auto B) {
        auto R = elem_fmax(A * B + C, var(0.0));
        return sum<0>(sum<0>(R));
    };

    check_gradient(func2, B);
}

TEST(GradientTest, GradientWrtTuple)
{
    auto shape_a = Shape(4, 10);
    auto shape_b = Shape(10, 5);
    auto shape_c = Shape(5);

    std::vector<double> a(shape_a.size());
    std::vector<double> b(shape_b.size());
    std::vector<double> c(shape_c.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });
    std::generate(c.begin(), c.end(), [&] { return dis(gen); });

    auto A = var(shape_a, a.begin());
    auto B = set_id<NumericId<0>>(var(shape_b, b.begin()));
    auto C = set_id<NumericId<1>>(var(shape_c, c.begin()));

    auto R = elem_fmax(A * B + C, var(0.0));
    auto S = sum<0>(sum<0>(R));
    auto G = grad(S, make_tuple(B, C));

    auto [gB, gC] = G.run(q);
    auto gBacc = gB.get_read_access();
    auto gCacc = gC.get_read_access();

    auto GB = grad(S, B);
    auto gB2 = GB.run(q);
    auto gBacc2 = gB2.get_read_access();

    auto GC = grad(S, C);
    auto gC2 = GC.run(q);
    auto gCacc2 = gC2.get_read_access();

    for (std::size_t i = 0; i < shape_b[0]; ++i) {
        for (std::size_t j = 0; j < shape_b[1]; ++j) {
            ASSERT_DOUBLE_EQ(gBacc[Index(i, j)], gBacc2[Index(i, j)]);
        }
    }

    for (std::size_t i = 0; i < shape_c[0]; ++i) {
        ASSERT_DOUBLE_EQ(gCacc[Index(i)], gCacc2[Index(i)]);
    }
}
