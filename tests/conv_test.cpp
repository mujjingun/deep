#include <gtest/gtest.h>

#include "grad_check.hpp"
#include "main.hpp"

using namespace cl::sycl;
using namespace dp;

struct Conv2DParams {
    std::size_t batch;
    std::size_t inHeight;
    std::size_t inWidth;
    std::size_t kHeight;
    std::size_t kWidth;
    std::size_t inChannels;
    std::size_t outChannels;
};

static void test_conv2d(Conv2DParams p)
{
    auto input_shape = Shape{ p.batch, p.inHeight, p.inWidth, p.inChannels };
    auto kernel_shape = Shape{ p.kHeight, p.kWidth, p.inChannels, p.outChannels };
    auto output_shape = Shape{ p.batch, p.inHeight - p.kHeight + 1, p.inWidth - p.kWidth + 1, p.outChannels };

    std::vector<double> a(input_shape.size());
    std::vector<double> k(kernel_shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(k.begin(), k.end(), [&] { return dis(gen); });

    // hand-rolled conv2d
    std::vector<double> r(output_shape.size());
    for (std::size_t batch = 0; batch < output_shape[0]; ++batch) {
        for (std::size_t ih = 0; ih < output_shape[1]; ++ih) {
            for (std::size_t iw = 0; iw < output_shape[2]; ++iw) {
                for (std::size_t o = 0; o < output_shape[3]; ++o) {
                    double ret = 0.0;
                    for (std::size_t c = 0; c < input_shape[3]; ++c) {
                        for (std::size_t h = 0; h < kernel_shape[0]; ++h) {
                            for (std::size_t w = 0; w < kernel_shape[1]; ++w) {
                                std::size_t a_idx = (((batch * input_shape[1] + (ih + h))) * input_shape[2] + (iw + w)) * input_shape[3] + c;
                                double input = a[a_idx];
                                std::size_t k_idx = (((h * kernel_shape[1] + w)) * kernel_shape[2] + c) * kernel_shape[3] + o;
                                double kernel = k[k_idx];
                                ret += input * kernel;
                            }
                        }
                    }
                    r[(((batch * output_shape[1] + ih)) * output_shape[2] + iw) * output_shape[3] + o] = ret;
                }
            }
        }
    }

    // library conv2d
    auto A = var(input_shape, a.begin());
    auto K = var(kernel_shape, k.begin());

    auto R = conv2d(A, K);
    auto buf = R.run(q);

    auto C = buf.get_read_access();

    for (std::size_t batch = 0; batch < output_shape[0]; ++batch) {
        for (std::size_t o = 0; o < output_shape[3]; ++o) {
            for (std::size_t ih = 0; ih < output_shape[1]; ++ih) {
                for (std::size_t iw = 0; iw < output_shape[2]; ++iw) {
                    double expected = r[(((batch * output_shape[1] + ih)) * output_shape[2] + iw) * output_shape[3] + o];
                    double actual = C[Index(batch, ih, iw, o)];
                    ASSERT_PRED2(near, expected, actual);
                }
            }
        }
    }
}
TEST(ConvTest, Conv2dSmall)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d({ 1, 3, 3, 2, 2, 1, 1 });
}
TEST(ConvTest, Conv2dBig)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d({ 1, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dMultipleChannels)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d({ 1, 20, 20, 5, 5, 4, 5 });
}
TEST(ConvTest, Conv2dBatches)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d({ 3, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dFull)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d({ 3, 20, 20, 5, 5, 4, 5 });
}

static void test_conv2d_backprop_filter(Conv2DParams p)
{
    auto input_shape = Shape{ p.batch, p.inHeight, p.inWidth, p.inChannels };
    auto kernel_shape = Shape{ p.kHeight, p.kWidth, p.inChannels, p.outChannels };
    auto output_shape = Shape{ p.batch, p.inHeight - p.kHeight + 1, p.inWidth - p.kWidth + 1, p.outChannels };

    std::vector<double> a(input_shape.size());
    std::vector<double> k(kernel_shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(k.begin(), k.end(), [&] { return dis(gen); });

    auto A = var(input_shape, a.begin());
    auto K = var(kernel_shape, k.begin());

    auto func = [&](auto K) {
        auto R = conv2d(A, K);
        auto s = sum_all(R);
        return s;
    };

    check_gradient(func, K);
}
TEST(ConvTest, Conv2dBPFilterSmall)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_filter({ 1, 3, 3, 2, 2, 1, 1 });
}
TEST(ConvTest, Conv2dBPFilterBig)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_filter({ 1, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dBPFilterMultipleChannels)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_filter({ 1, 20, 20, 5, 5, 4, 5 });
}
TEST(ConvTest, Conv2dBPFilterBatches)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_filter({ 3, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dBPFilterFull)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_filter({ 3, 20, 20, 5, 5, 4, 5 });
}

static void test_conv2d_backprop_input(Conv2DParams p)
{
    auto input_shape = Shape{ p.batch, p.inHeight, p.inWidth, p.inChannels };
    auto kernel_shape = Shape{ p.kHeight, p.kWidth, p.inChannels, p.outChannels };
    auto output_shape = Shape{ p.batch, p.inHeight - p.kHeight + 1, p.inWidth - p.kWidth + 1, p.outChannels };

    std::vector<double> a(input_shape.size());
    std::vector<double> k(kernel_shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(k.begin(), k.end(), [&] { return dis(gen); });

    auto A = var(input_shape, a.begin());
    auto K = var(kernel_shape, k.begin());

    auto func = [&](auto A) {
        auto R = conv2d(A, K);
        auto s = sum_all(R);
        return s;
    };

    check_gradient(func, A);
}
TEST(ConvTest, Conv2dBPInputSmall)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_input({ 1, 3, 3, 2, 2, 1, 1 });
}
TEST(ConvTest, Conv2dBPInputBig)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_input({ 1, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dBPInputMultipleChannels)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_input({ 1, 20, 20, 5, 5, 4, 5 });
}
TEST(ConvTest, Conv2dBPInputBatches)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_input({ 3, 20, 20, 5, 5, 1, 1 });
}
TEST(ConvTest, Conv2dBPInputFull)
{
    //batch;inHeight;inWidth;kHeight;kWidth;inChannels;outChannels;
    test_conv2d_backprop_input({ 3, 20, 20, 5, 5, 4, 5 });
}

TEST(PoolingTest, PoolingSimple)
{
    Shape<4> input_shape{ 1, 4, 4, 1 };
    std::vector<double> a = {
        3, 1, 2, 5, //
        2, 0, 2, 4, //
        0, 2, 0, 1, //
        0, 1, 3, 9, //
    };

    auto A = var(input_shape, a.begin());
    auto pooled = max_pooling(A, Shape{ 2, 2 });

    auto buf = pooled.run(q);
    auto acc = buf.get_read_access();

    ASSERT_DOUBLE_EQ(acc[Index(0, 0, 0, 0)], 3.0);
    ASSERT_DOUBLE_EQ(acc[Index(0, 0, 1, 0)], 5.0);
    ASSERT_DOUBLE_EQ(acc[Index(0, 1, 0, 0)], 2.0);
    ASSERT_DOUBLE_EQ(acc[Index(0, 1, 1, 0)], 9.0);
}

void pooling_test(Shape<4> shape, Shape<2> kernel)
{
    std::vector<double> a(shape.size());

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });

    auto A = var(shape, a.begin());
    auto pooled = max_pooling(A, kernel);

    auto buf = pooled.run(q);
    auto acc = buf.get_read_access();

    Shape<4> new_shape{ shape[0], shape[1] / kernel[0], shape[2] / kernel[1], shape[3] };
    std::vector<double> r(new_shape.size());

    for (std::size_t i = 0; i < new_shape[0]; ++i) {
        for (std::size_t l = 0; l < new_shape[3]; ++l) {
            for (std::size_t j = 0; j < new_shape[1]; ++j) {
                for (std::size_t k = 0; k < new_shape[2]; ++k) {
                    double val = -std::numeric_limits<double>::infinity();
                    for (std::size_t x = 0; x < kernel[0]; ++x) {
                        for (std::size_t y = 0; y < kernel[1]; ++y) {
                            std::size_t z = j * kernel[0] + x;
                            std::size_t w = k * kernel[1] + y;
                            val = std::max(val, a[((i * shape[1] + z) * shape[2] + w) * shape[3] + l]);
                        }
                    }
                    r[((i * new_shape[1] + j) * new_shape[2] + k) * new_shape[3] + l] = val;
                }
            }
        }
    }

    ASSERT_EQ(acc.shape(), new_shape);

    for (std::size_t i = 0; i < new_shape[0]; ++i) {
        for (std::size_t j = 0; j < new_shape[1]; ++j) {
            for (std::size_t k = 0; k < new_shape[2]; ++k) {
                for (std::size_t l = 0; l < new_shape[3]; ++l) {
                    double expected = r[((i * new_shape[1] + j) * new_shape[2] + k) * new_shape[3] + l];
                    double actual = acc[Index{ i, j, k, l }];
                    ASSERT_PRED2(near, expected, actual);
                }
            }
        }
    }
}

TEST(PoolingTest, PoolingFull)
{
    pooling_test(Shape{ 3, 60, 80, 4 }, Shape{ 3, 4 });
}
