#ifndef GRAD_CHECK_HPP
#define GRAD_CHECK_HPP

#include <gradients.hpp>
#include <gtest/gtest.h>
#include <iostream>
#include <random>
#include <set>

#include "main.hpp"

using namespace cl::sycl;
using namespace dp;

template <long long Id>
class NumericId;

inline bool near(double a, double b)
{
    double abserr = std::fabs(a - b);
    double denom = std::fabs((a + b) / 2);
    double relerr = abserr / denom;
    return abserr < 1e-5 || relerr < 1e-3;
}

template <std::size_t Samples = 20, typename F,
    typename ValueType, std::size_t Rank, typename Expr>
static auto check_gradient(F const& func, Tensor<Expr, ValueType, Rank> const& input, Queue& qu = q)
{
    constexpr double epsilon = 1e-6;

    auto gen = std::mt19937(std::random_device{}());

    auto A = set_id<NumericId<0>>(~input);
    auto y = func(A);
    auto g = grad(y, A);

    auto G = g.run(qu);
    auto gradient = G.get_read_access();

    auto shape = G.shape();

    [&] { ASSERT_EQ(shape, A.get().shape); }();

    auto N = shape.size();
    auto k = std::min(N, Samples);

    // sample k samples
    std::set<std::size_t> set;
    for (std::size_t r = N - k; r < N; ++r) {
        auto v = std::uniform_int_distribution<std::size_t>(0, r)(gen);
        if (!set.count(v)) {
            set.insert(v);
        } else {
            set.insert(r);
        }
    }

    for (std::size_t i : set) {
        auto idx = unflatten_index(i, shape);

        Shape<Rank> ones;
        std::fill(ones.begin(), ones.end(), std::size_t(1));

        auto delta = reshape(var(epsilon), ones);

        Shape<Rank> pad_before, pad_after;
        for (std::size_t j = 0; j < Rank; ++j) {
            pad_before[j] = idx[j];
            pad_after[j] = shape[j] - idx[j] - 1;
        }

        auto padded = pad(delta, pad_before, pad_after);
        auto yp = func(reshape_like(A + padded, A));
        auto yn = func(reshape_like(A - padded, A));
        auto gn = (yp - yn) / var(epsilon * 2);

        auto AD_value = gradient[idx];
        auto numerical_gradient = gn.run(qu).get_read_access()[Index{}];

        [&] { ASSERT_PRED2(near, AD_value, numerical_gradient); }();
    }

    return gradient;
}

#endif // GRAD_CHECK_HPP
