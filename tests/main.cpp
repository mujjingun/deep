#include "main.hpp"

#include <gtest/gtest.h>

#include <deep.hpp>

using namespace cl::sycl;
using namespace dp;

using selector = intel_selector;

// Create a queue to work on
Queue q(selector{});

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(BasicTest, Hold)
{
    auto h = Hold{ 0, 1, 2, 3 };
    h.apply([](auto... i) {
        ASSERT_EQ(std::make_tuple(i...), std::make_tuple(0, 1, 2, 3));
    });
}

static_assert(is_dimexpr<FixedDimExpr<>>);
static_assert(!is_dimexpr<int>);
