#ifndef GRADIENTS_HPP
#define GRADIENTS_HPP

#include "expressions.hpp"
#include "type_utils.hpp"

namespace dp {

// Gradients wrt its n inputs
// expr = out, dout = dy/dout
// Returns tuple{ dy/darg0, dy/darg1, ... }
template <typename ExprType, typename ValueType, std::size_t Rank,
    typename WrtExprType, typename WrtValueType, std::size_t WrtRank>
inline auto diff(
    Tensor<ExprType, ValueType, Rank> const& expr,
    Tensor<WrtExprType, WrtValueType, WrtRank> const& dout)
{
    using Tag = typename ExprType::Tag;
    if constexpr (std::is_same_v<Tag, AddDimensionTag>) {
        return std::make_tuple(reshape_like(~dout, expr.arg0()));
    } else if constexpr (std::is_same_v<Tag, Conv2DTag>) {
        return std::make_tuple(
            conv2d_backprop_input(expr.arg1(), ~dout),
            conv2d_backprop_filter(expr.arg0(), ~dout));
    } else if constexpr (std::is_same_v<Tag, ElemwiseUnaryTensorTag>) {
        using Operator = typename ExprType::Operator;
        if constexpr (std::is_same_v<Operator, internal::functor_negate>) {
            return std::make_tuple(-(~dout));
        } else if constexpr (std::is_same_v<Operator, internal::functor_exp>) {
            return std::make_tuple(elem_mul(~dout, ~expr));
        } else if constexpr (std::is_same_v<Operator, internal::functor_log>) {
            return std::make_tuple(elem_div(~dout, expr.arg0()));
        } else if constexpr (internal::is_cast<Operator>) {
            using OrigType = typename std::decay_t<decltype(expr.arg0())>::ValueType;
            return std::make_tuple(cast<OrigType>(~dout));
        } else {
            static_assert(ExprType::num_args == 0, "diff() not implemented for this op");
        }
    } else if constexpr (std::is_same_v<Tag, ElemwiseTensorTag>) {
        using Operator = typename ExprType::Operator;
        if constexpr (std::is_same_v<Operator, internal::functor_plus>) {
            return std::make_tuple(
                sum_like(~dout, expr.arg0()),
                sum_like(~dout, expr.arg1()));
        } else if constexpr (std::is_same_v<Operator, internal::functor_multiplies>) {
            return std::make_tuple(
                sum_like(elem_mul(~dout, expr.arg1()), expr.arg0()),
                sum_like(elem_mul(~dout, expr.arg0()), expr.arg1()));
        } else if constexpr (std::is_same_v<Operator, internal::functor_divides>) {
            return std::make_tuple(
                sum_like(elem_div(~dout, expr.arg1()), expr.arg0()),
                sum_like(elem_mul(~dout, -elem_div(expr.arg0(), elem_mul(expr.arg1(), expr.arg1()))), expr.arg1()));
        } else if constexpr (std::is_same_v<Operator, internal::functor_fmax>) {
            return std::make_tuple(
                select(expr.arg0() > expr.arg1(), sum_like(~dout, expr.arg0()), zeros_like(expr.arg0())),
                select(expr.arg1() > expr.arg0(), sum_like(~dout, expr.arg1()), zeros_like(expr.arg1())));
        } else {
            static_assert(ExprType::num_args == 0, "diff() not implemented for this op");
        }
    } else if constexpr (std::is_same_v<Tag, IndexExprTag>) {
        return std::make_tuple(index_backprop(~dout, shape_of(expr.arg0()), (~expr).m_indices));
    } else if constexpr (std::is_same_v<Tag, MultMatrixMatrixTag>) {
        return std::make_tuple(
            (~dout) * transpose<0, 1>(expr.arg1()),
            transpose<0, 1>(expr.arg0()) * (~dout));
    } else if constexpr (std::is_same_v<Tag, ReduceExprTag>) {
        using Operator = typename ExprType::Operator;
        constexpr auto AlongDim = ExprType::AlongDim;

        auto expanded_dout = [&] {
            if constexpr (ExprType::KeepDim) {
                return ~dout;
            } else {
                return add_dim<AlongDim>(~dout);
            }
        }();

        auto expanded_expr = [&] {
            if constexpr (ExprType::KeepDim) {
                return ~expr;
            } else {
                return add_dim<AlongDim>(~expr);
            }
        }();

        if constexpr (std::is_same_v<Operator, internal::functor_plus>) {
            return std::make_tuple(tile_like<AlongDim>(expanded_dout, expr.arg0()));
        } else if constexpr (std::is_same_v<Operator, internal::functor_fmax>) {
            return std::make_tuple(select(expr.arg0() == expanded_expr, expanded_dout, zeros_like(expr.arg0())));
        } else if constexpr (std::is_same_v<Operator, internal::functor_fmin>) {
            return std::make_tuple(select(expr.arg0() == expanded_expr, expanded_dout, zeros_like(expr.arg0())));
        } else {
            static_assert(ExprType::num_args == 0, "diff() not implemented for this op");
        }
    } else if constexpr (std::is_same_v<Tag, ReshapeTag>) {
        return std::make_tuple(reshape_like(~dout, expr.arg0()));
    } else if constexpr (std::is_same_v<Tag, SetShapeTag>) {
        return std::make_tuple(ensure_shape_like(~dout, expr));
    } else if constexpr (std::is_same_v<Tag, TileTensorTag>) {
        return std::make_tuple(sum_keep_dim<ExprType::Dim>(~dout));
    } else if constexpr (std::is_same_v<Tag, TransposeTensorTag>) {
        return std::make_tuple(transpose<ExprType::Dim1, ExprType::Dim2>(~dout));
    } else {
        static_assert(ExprType::num_args == 0, "diff() not implemented for this op");
        return std::make_tuple();
    }
}

namespace internal {
    /**
     * Build a symbolic graph of the gradient of op wrt ph.
     * returns dy/di = dy/d(op) * d(op)/di, given grads = dy/d(op), where y is a
     * scalar function of i, and i is the input(s) of op.
     * y = f(op(i))
     * expr = op
     * grads = dy/d(op)
     */
    template <typename ExprType, typename GradsType,
        typename... WrtElemTypes>
    inline auto grad_impl(
        ExprBase<ExprType> const& expr,
        GradsType const& grads,
        TupleExpr<WrtElemTypes...> const& wrts)
    {
        static_assert((... && !std::is_same_v<typename LeafType<WrtElemTypes>::Id, InvalidId>),
            "Wrt must have a valid id");

        auto d_ins = [&] {
            if constexpr (ExprType::num_args == 0) {
                return std::apply([](auto const&... wrt) {
                    return dp::make_tuple(zeros_like(~wrt)...);
                },
                    wrts);
            } else {
                auto w_bar = diff(~expr, ~grads);
                auto sum_grads = [&wrts](auto const&... pairs) {
                    return (grad_impl(std::get<0>(pairs), std::get<1>(pairs), ~wrts) + ...);
                };
                return apply(sum_grads, (~expr).m_args, w_bar);
            }
        }();

        auto func = [&](auto const& pair) {
            auto const& [wrt, d_in] = pair;
            if constexpr (std::is_same_v<
                              typename std::decay_t<decltype(~wrt)>::Id,
                              typename LeafType<ExprType>::Id>) {
                return ~grads;
            } else {
                return d_in;
            }
        };

        return apply([&](auto const&... pairs) {
            return dp::make_tuple(func(pairs)...);
        },
            wrts, d_ins);
    }
}

template <typename ExprType, typename ValueType,
    typename... WrtElemTypes>
inline auto grad(Tensor<ExprType, ValueType, 0> const& expr,
    TupleExpr<WrtElemTypes...> const& wrts)
{
    return internal::grad_impl(~expr, ones<ValueType>(Shape{}), ~wrts);
}

template <typename ExprType, typename ValueType,
    typename WrtExpr, typename WrtValueType, std::size_t WrtRank>
inline auto grad(Tensor<ExprType, ValueType, 0> const& expr,
    Tensor<WrtExpr, WrtValueType, WrtRank> const& wrt)
{
    return internal::grad_impl(~expr, ones<ValueType>(Shape{}), dp::make_tuple(~wrt)).arg0();
}
}

#endif // GRADIENTS_HPP
