#ifndef VOCABTYPES_HPP
#define VOCABTYPES_HPP

#include <CL/sycl.hpp>
#include <numeric>

#include "macros.hpp"

namespace dp {

template <std::size_t Rank>
struct MDBase {
    static constexpr auto rank = Rank;

    std::array<std::size_t, Rank> m_idx{ {} };

    constexpr MDBase()
    {
    }

    template <typename... Args, typename = typename std::enable_if_t<sizeof...(Args) == Rank>>
    constexpr explicit MDBase(Args... args)
        : m_idx{ { static_cast<std::size_t>(args)... } }
    {
    }
    constexpr explicit MDBase(std::array<std::size_t, Rank> idx)
        : m_idx(idx)
    {
    }

    constexpr std::size_t const& operator[](std::size_t i) const
    {
        D_ASSERT(i < Rank, "Index out of range");
        return m_idx[i];
    }
    constexpr std::size_t& operator[](std::size_t i)
    {
        D_ASSERT(i < Rank, "Index out of range");
        return m_idx[i];
    }

    constexpr bool operator==(MDBase<Rank> const& other) const
    {
        return m_idx == other.m_idx;
    }

    constexpr auto begin() const
    {
        return m_idx.begin();
    }

    constexpr auto begin()
    {
        return m_idx.begin();
    }

    constexpr auto end() const
    {
        return m_idx.end();
    }

    constexpr auto end()
    {
        return m_idx.end();
    }
};

template <std::size_t Rank>
struct Shape;

template <std::size_t Rank>
struct Index : MDBase<Rank> {
    using MDBase<Rank>::MDBase;

    friend std::ostream& operator<<(std::ostream& output, Index const& idx)
    {
        output << "Index{ ";
        for (std::size_t i = 0; i < Rank; ++i) {
            output << idx[i];
            if (i < Rank - 1) {
                output << ", ";
            }
        }
        output << " }";
        return output;
    }
};

template <typename... Args>
Index(Args... args)->Index<sizeof...(Args)>;
Index()->Index<0>;

inline constexpr std::size_t UnknownDim = 0x7fffffff;

template <std::size_t Rank>
struct Shape : Index<Rank> {
    using Index<Rank>::Index;

    inline constexpr std::size_t size() const noexcept;

    friend std::ostream& operator<<(std::ostream& output, Shape const& shp)
    {
        output << "Shape{ ";
        for (std::size_t i = 0; i < Rank; ++i) {
            output << shp[i];
            if (i < Rank - 1) {
                output << ", ";
            }
        }
        output << " }";
        return output;
    }

    constexpr bool is_match(Shape<Rank> shape) const
    {
        for (std::size_t i = 0; i < Rank; ++i) {
            if ((*this)[i] != UnknownDim && (*this)[i] != shape[i]) {
                return false;
            }
        }
        return true;
    }
};

template <typename... Args>
Shape(Args... args)->Shape<sizeof...(Args)>;
Shape()->Shape<0>;

template <std::size_t Rank>
inline constexpr std::size_t Shape<Rank>::size() const noexcept
{
    std::size_t ret = 1;
    for (auto i : Index<Rank>::m_idx) {
        ret *= i;
    }
    return ret;
}

template <std::size_t Rank>
constexpr auto flatten_index(Index<Rank> idx, Shape<Rank> shape)
{
    std::size_t ret = 0;
    for (std::size_t i = 0; i < Rank; ++i) {
        ret = ret * shape[i] + idx[i];
    }
    return ret;
}

template <std::size_t Rank>
constexpr auto unflatten_index(std::size_t idx, Shape<Rank> shape)
{
    Index<Rank> ret;
    for (std::size_t i = Rank - 1; i < Rank; --i) {
        ret[i] = idx % shape[i];
        idx /= shape[i];
    }
    return ret;
}

template <std::size_t Rank>
constexpr auto to_range(Shape<Rank> shape)
{
    if constexpr (Rank == 0) {
        return cl::sycl::range<1>(1);
    } else if constexpr (Rank == 1) {
        return cl::sycl::range<1>(shape[0]);
    } else if constexpr (Rank == 2) {
        return cl::sycl::range<2>(shape[0], shape[1]);
    } else if constexpr (Rank == 3) {
        return cl::sycl::range<3>(shape[0], shape[1], shape[2]);
    } else if constexpr (Rank == 4) {
        return cl::sycl::range<3>(shape[0] * shape[1], shape[2], shape[3]);
    } else {
        // squeeze all dimensions above 1
        std::size_t dim0 = shape[0];
        for (std::size_t i = 1; i < Rank - 2; ++i) {
            dim0 *= shape[i];
        }
        return cl::sycl::range<3>(dim0, shape[Rank - 2], shape[Rank - 1]);
    }
}

template <std::size_t Rank>
constexpr auto to_id(Index<Rank> idx, Shape<Rank> shape)
{
    if constexpr (Rank == 0) {
        return cl::sycl::id<1>(0);
    } else if constexpr (Rank == 1) {
        return cl::sycl::id<1>(idx[0]);
    } else if constexpr (Rank == 2) {
        return cl::sycl::id<2>(idx[0], idx[1]);
    } else if constexpr (Rank == 3) {
        return cl::sycl::id<3>(idx[0], idx[1], idx[2]);
    } else if constexpr (Rank == 4) {
        return cl::sycl::id<3>(idx[0] * shape[1] + idx[1], idx[2], idx[3]);
    } else {
        std::size_t dim0 = idx[0];
        for (std::size_t i = 1; i < Rank - 2; ++i) {
            dim0 = dim0 * shape[i] + idx[i];
        }
        return cl::sycl::id<3>(dim0, idx[Rank - 2], idx[Rank - 1]);
    }
}

template <std::size_t Rank, int ClRank>
constexpr Index<Rank> to_index(cl::sycl::id<ClRank> id, Shape<Rank> shape)
{
    if constexpr (Rank == 0) {
        return Index<0>{};
    } else if constexpr (Rank == 1) {
        return Index<1>{ id[0] };
    } else if constexpr (Rank == 2) {
        return Index<2>{ id[0], id[1] };
    } else if constexpr (Rank == 3) {
        return Index<3>{ id[0], id[1], id[2] };
    } else {
        Index<Rank> ret;

        std::size_t t = id[0];
        for (std::size_t i = Rank - 3; i > 0; --i) {
            ret[i] = t % shape[i];
            t /= shape[i];
        }
        ret[0] = t;

        ret[Rank - 2] = id[1];
        ret[Rank - 1] = id[2];

        return ret;
    }
}

template <std::size_t... Dims>
struct StaticShape {
    constexpr static std::size_t Rank = sizeof...(Dims);
    static constexpr Shape<Rank> to_shape()
    {
        return Shape{ Dims... };
    }
};

namespace internal {
    template <std::size_t... Is>
    inline constexpr StaticShape<(Is, UnknownDim)...> make_unknown_shape_impl(std::index_sequence<Is...>) { return {}; }
}
template <std::size_t Rank>
using make_unknown_shape = decltype(internal::make_unknown_shape_impl(std::make_index_sequence<Rank>{}));

namespace internal {
    template <std::size_t... Dims>
    inline constexpr std::true_type is_static_shape_impl(StaticShape<Dims...>) { return {}; }
    template <typename T>
    inline constexpr std::false_type is_static_shape_impl(T const&) { return {}; }
}
template <typename T>
inline constexpr bool is_static_shape = decltype(internal::is_static_shape_impl(std::declval<T>()))::value;
}

namespace std {
template <std::size_t I, std::size_t Rank>
constexpr std::size_t&& get(dp::Index<Rank>&& v) noexcept
{
    return v[I];
}
template <std::size_t I, std::size_t Rank>
constexpr std::size_t& get(dp::Index<Rank>& v) noexcept
{
    return v[I];
}
template <std::size_t I, std::size_t Rank>
constexpr std::size_t const& get(dp::Index<Rank> const& v) noexcept
{
    return v[I];
}
template <std::size_t Rank>
struct tuple_size<dp::Index<Rank>> : std::integral_constant<std::size_t, Rank> {
};

template <std::size_t I, std::size_t Rank>
constexpr std::size_t&& get(dp::Shape<Rank>&& v) noexcept
{
    return v[I];
}
template <std::size_t I, std::size_t Rank>
constexpr std::size_t& get(dp::Shape<Rank>& v) noexcept
{
    return v[I];
}
template <std::size_t I, std::size_t Rank>
constexpr std::size_t const& get(dp::Shape<Rank> const& v) noexcept
{
    return v[I];
}
template <std::size_t Rank>
struct tuple_size<dp::Shape<Rank>> : std::integral_constant<std::size_t, Rank> {
};
}

#endif // VOCABTYPES_HPP
