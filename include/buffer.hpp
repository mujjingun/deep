#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <CL/sycl.hpp>
#include <iostream>

#include "shapes.hpp"

namespace dp {

class Queue {
    cl::sycl::queue m_queue;

public:
    template <typename... Ts>
    Queue(Ts&&... args)
        : m_queue(std::forward<Ts>(args)...)
    {
    }

    template <typename Functor>
    decltype(auto) submit(Functor&& func)
    {
        return m_queue.submit(std::forward<Functor>(func));
    }
};

template <typename SyclAccessor, typename ValueType, std::size_t Rank, typename Enable = void>
struct ReadAccessor {
    SyclAccessor m_acc;
    Shape<Rank> m_shape;

    explicit ReadAccessor(SyclAccessor acc, Shape<Rank> shape)
        : m_acc(acc)
        , m_shape(shape)
    {
    }

    ValueType operator[](Index<Rank> idx) const
    {
        return m_acc[to_id(idx, m_shape)];
    }
};

template <typename SyclAccessor, typename ValueType, std::size_t Rank>
struct ReadAccessor<SyclAccessor, ValueType, Rank, typename std::enable_if_t<Rank <= 3>> {
    SyclAccessor m_acc;

    explicit ReadAccessor(SyclAccessor acc, Shape<Rank>)
        : m_acc(acc)
    {
    }

    ValueType operator[](Index<Rank> idx) const
    {
        if constexpr (Rank == 0) {
            return m_acc[0];
        } else if constexpr (Rank == 1) {
            return m_acc[idx[0]];
        } else if constexpr (Rank == 2) {
            return m_acc[idx[0]][idx[1]];
        } else if constexpr (Rank == 3) {
            return m_acc[idx[0]][idx[1]][idx[2]];
        }
    }
};

template <typename SyclAccessor, typename ValueType, std::size_t Rank, typename Enable = void>
struct ReadWriteAccessor {
    SyclAccessor m_acc;
    Shape<Rank> m_shape;

    explicit ReadWriteAccessor(SyclAccessor acc, Shape<Rank> shape)
        : m_acc(acc)
        , m_shape(shape)
    {
    }

    ValueType& operator[](Index<Rank> idx) const
    {
        return m_acc[to_id(idx, m_shape)];
    }

    template <int ClRank>
    ValueType& by_id(cl::sycl::id<ClRank> const& id) const
    {
        return m_acc[id];
    }

    template <int ClRank>
    Index<Rank> to_index(cl::sycl::id<ClRank> const& id) const
    {
        Index<Rank> ret;

        std::size_t t = id[0];
        for (std::size_t i = Rank - 3; i > 0; --i) {
            ret[i] = t % m_shape[i];
            t /= m_shape[i];
        }
        ret[0] = t;

        ret[Rank - 2] = id[1];
        ret[Rank - 1] = id[2];

        return ret;
    }
};

template <typename SyclAccessor, typename ValueType, std::size_t Rank>
struct ReadWriteAccessor<SyclAccessor, ValueType, Rank, typename std::enable_if_t<Rank <= 3>> {
    SyclAccessor m_acc;

    explicit ReadWriteAccessor(SyclAccessor acc, Shape<Rank>)
        : m_acc(acc)
    {
    }

    ValueType& operator[](Index<Rank> idx) const
    {
        if constexpr (Rank == 0) {
            return m_acc[0];
        } else if constexpr (Rank == 1) {
            return m_acc[idx[0]];
        } else if constexpr (Rank == 2) {
            return m_acc[idx[0]][idx[1]];
        } else if constexpr (Rank == 3) {
            return m_acc[idx[0]][idx[1]][idx[2]];
        }
    }

    template <int ClRank>
    ValueType& by_id(cl::sycl::id<ClRank> const& id) const
    {
        return m_acc[id];
    }

    template <int ClRank>
    Index<Rank> to_index(cl::sycl::id<ClRank> const& id) const
    {
        if constexpr (Rank == 0) {
            return Index<0>{};
        } else if constexpr (Rank == 1) {
            return Index<1>{ id[0] };
        } else if constexpr (Rank == 2) {
            return Index<2>{ id[0], id[1] };
        } else if constexpr (Rank == 3) {
            return Index<3>{ id[0], id[1], id[2] };
        }
    }
};

template <typename SyclAccessor, typename ValueType, std::size_t Rank>
class HostAccessor {
    SyclAccessor m_acc;
    Shape<Rank> m_shape;

public:
    explicit HostAccessor(SyclAccessor acc, Shape<Rank> shape)
        : m_acc(acc)
        , m_shape(shape)
    {
    }

    decltype(auto) operator[](Index<Rank> idx) const
    {
        return m_acc[to_id(idx, m_shape)];
    }

    Shape<Rank> shape() const
    {
        return m_shape;
    }

    friend std::ostream& operator<<(std::ostream& output, HostAccessor const& acc)
    {
        Index<Rank> idx;
        if constexpr (Rank == 0) {
            output << acc[idx];
        } else {
            while (idx[0] < acc.m_shape[0]) {
                output << acc[idx] << " ";

                ++idx[Rank - 1];
                std::size_t j = Rank - 1;
                while (j > 0 && idx[j] >= acc.m_shape[j]) {
                    idx[j] = 0;
                    ++idx[--j];

                    output << "\n";
                }
            }
        }
        return output;
    }
};

template <std::size_t Rank>
inline constexpr std::size_t ClSize = std::clamp<std::size_t>(Rank, 1, 3);

template <std::size_t Rank>
using ClIdType = cl::sycl::id<ClSize<Rank>>;

template <typename ValueType, std::size_t Rank>
class Buffer {
    static constexpr std::size_t cl_size = ClSize<Rank>;
    using BufType = cl::sycl::buffer<ValueType, cl_size>;
    using IdType = cl::sycl::id<cl_size>;

private:
    std::shared_ptr<ValueType[]> m_hostvec;
    mutable BufType m_buf;
    Shape<Rank> m_shape;

    template <typename Iterator>
    static std::shared_ptr<ValueType[]> copy_data(Iterator begin, std::size_t size)
    {
        D_ASSERT(size > 0, "Cannot create a buffer with size == 0");
        std::shared_ptr<ValueType[]> ret(new ValueType[size]);
        std::copy_n(begin, size, ret.get());
        return ret;
    }

public:
    Buffer(Shape<Rank> shape)
        : m_buf(to_range(shape))
        , m_shape(shape)
    {
    }

    template <typename Iterator>
    Buffer(Shape<Rank> shape, Iterator begin)
        : m_hostvec(copy_data(begin, shape.size()))
        , m_buf(static_cast<ValueType const*>(m_hostvec.get()), to_range(shape))
        , m_shape(shape)
    {
    }

    Shape<Rank> shape() const
    {
        return m_shape;
    }

    auto get_read_access(cl::sycl::handler& cgh) const
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::read>(cgh);
        return ReadAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }
    auto get_write_access(cl::sycl::handler& cgh)
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::write>(cgh);
        return ReadWriteAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }
    auto get_rw_access(cl::sycl::handler& cgh)
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::read_write>(cgh);
        return ReadWriteAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }

    auto get_read_access() const
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::read>();
        return HostAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }
    auto get_write_access()
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::write>();
        return HostAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }
    auto get_rw_access()
    {
        using namespace cl::sycl;
        auto acc = m_buf.template get_access<access::mode::read_write>();
        return HostAccessor<decltype(acc), ValueType, Rank>{ acc, m_shape };
    }
};
} // namespace dp

#endif // BUFFER_HPP
