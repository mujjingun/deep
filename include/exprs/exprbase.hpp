#ifndef EXPRBASE_HPP
#define EXPRBASE_HPP

#include "buffer.hpp"
#include "type_utils.hpp"

#include <any>

namespace dp {

// The default id for most expressions
class InvalidId;

struct ExprState {
    std::size_t eval_cnt;
    std::size_t num_total_evals;
    std::any data;
};

template <typename Expr>
using LeafType = std::decay_t<decltype(~std::declval<Expr>())>;

template <typename Child>
struct ExprBase {
    using Id = InvalidId;

    constexpr static std::size_t num_args = Child::num_args;

    template <std::size_t N>
    constexpr auto& arg()
    {
        return (~*this).template arg<N>();
    }

    template <std::size_t N>
    constexpr const auto& arg() const
    {
        return (~*this).template arg<N>();
    }

    constexpr auto& arg0() { return arg<0>(); }
    constexpr const auto& arg0() const { return arg<0>(); }
    constexpr auto& arg1() { return arg<1>(); }
    constexpr const auto& arg1() const { return arg<1>(); }
    constexpr auto& arg2() { return arg<2>(); }
    constexpr const auto& arg2() const { return arg<2>(); }
    constexpr auto& arg3() { return arg<3>(); }
    constexpr const auto& arg3() const { return arg<3>(); }

    constexpr decltype(auto) operator~() const
    {
        return ~static_cast<Child const&>(*this);
    }

    constexpr decltype(auto) operator~()
    {
        return ~static_cast<Child&>(*this);
    }

    std::shared_ptr<ExprState> m_state = std::make_shared<ExprState>();

    template <typename F, std::size_t... Is>
    void for_each_arg_impl(F&& f, std::index_sequence<Is...>) const { (f(this->arg<Is>(), Is), ...); }
    template <typename F>
    void for_each_arg(F&& f) const { for_each_arg_impl(std::forward<F>(f), std::make_index_sequence<num_args>{}); }
    template <typename F, std::size_t... Is>
    void for_each_arg_impl(F&& f, std::index_sequence<Is...>) { (f(this->arg<Is>(), Is), ...); }
    template <typename F>
    void for_each_arg(F&& f) { for_each_arg_impl(std::forward<F>(f), std::make_index_sequence<num_args>{}); }

    void reset_state()
    {
        m_state->eval_cnt = 0;
        m_state->num_total_evals = 0;
        m_state->data.reset();
        for_each_arg([](auto& arg, auto) {
            arg.reset_state();
        });
    }

    void count_evals()
    {
        m_state->num_total_evals++;
        for_each_arg([](auto& arg, auto) {
            arg.count_evals();
        });
    }

    auto run(Queue& q)
    {
        reset_state();
        count_evals();
        return (~*this).run_impl(q);
    }

    std::string name() const
    {
        return typeid(decltype(*this)).name();
    }

    void print(std::ostream& output, std::size_t level) const
    {
        auto indent = [&](std::size_t n) {
            for (std::size_t i = 0; i < n; ++i) {
                output << " ";
            }
        };
        if (num_args > 0) {
            output << (~*this).name() << "(\n";
            for_each_arg([&](auto const& arg, auto i) {
                if (i > 0) {
                    output << ",\n";
                }
                indent(level + 1);
                arg.print(output, level + 1);
            });
            output << "\n";
            indent(level);
            output << ")";
        } else {
            output << (~*this).name();
        }
    }

    friend std::ostream& operator<<(std::ostream& output, ExprBase const& expr)
    {
        expr.print(output, 0);
        return output;
    }
};

// Hashing
inline constexpr std::uint64_t hash_int(std::uint64_t x)
{
    x ^= x >> 32;
    x *= UINT64_C(0xd6e8feb86659fd93);
    x ^= x >> 32;
    x *= UINT64_C(0xd6e8feb86659fd93);
    x ^= x >> 32;
    return x;
}

inline constexpr std::uint64_t hash_str(const char* str)
{
    std::size_t i = 0;
    std::size_t x = UINT64_C(0xbf58476d1ce4e5b9);
    for (; str[i]; ++i) {
        x += str[i];
        x = hash_int(x);
    }
    x ^= i;
    x = hash_int(x);
    return x + UINT64_C(0xbf58476d1ce4e5b9);
}

template <typename T>
inline constexpr std::uint64_t hash_type()
{
    if constexpr (std::is_same_v<T, float>) {
        return hash_str("float");
    } else if constexpr (std::is_same_v<T, double>) {
        return hash_str("double");
    } else if constexpr (std::is_same_v<T, bool>) {
        return hash_str("bool");
    } else if constexpr (std::is_same_v<T, int>) {
        return hash_str("int");
    } else if constexpr (std::is_same_v<T, unsigned int>) {
        return hash_str("unsigned int");
    } else if constexpr (std::is_same_v<T, long>) {
        return hash_str("long");
    } else if constexpr (std::is_same_v<T, unsigned long>) {
        return hash_str("unsigned long");
    } else if constexpr (std::is_same_v<T, short>) {
        return hash_str("short");
    } else if constexpr (std::is_same_v<T, unsigned short>) {
        return hash_str("unsigned short");
    } else {
        static_assert(std::is_same_v<T, double>);
    }
}

template <typename... Ts>
inline constexpr std::uint64_t hash_combine(std::uint64_t x, Ts... args)
{
    ((x = hash_int(x ^ static_cast<std::uint64_t>(args))), ...);
    x ^= sizeof...(args);
    x = hash_int(x);
    return x;
}
}

#endif // EXPRBASE_HPP
