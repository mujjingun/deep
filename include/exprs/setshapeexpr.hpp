#ifndef SETSHAPEEXPR_HPP
#define SETSHAPEEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

class SetShapeTag;
template <typename ChildExpr, typename ShapeType>
struct SetShape : Tensor<SetShape<ChildExpr, ShapeType>,
                      typename ChildExpr::ValueType,
                      ChildExpr::rank> {
    using SShape = typename ShapeType::SShape;
    using Tag = SetShapeTag;
    std::tuple<ChildExpr> m_args;
    ShapeType m_shape;

    explicit SetShape(
        ChildExpr const& expr,
        ShapeType const& shape)
        : m_args(~expr)
        , m_shape(~shape)
    {
    }

    auto build() const
    {
        auto [shape, a] = this->arg0().get();
        D_ASSERT(m_shape.get().is_match(shape), "Shapes do not match");
        return make_pair(shape, a);
    }

    std::string name() const
    {
        return std::string("SetShape");
    }

    constexpr static std::uint64_t hash()
    {
        return ChildExpr::hash();
    }
};

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto ensure_shape(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    Shape<Rank> shape)
{
    auto lshape = lazy_shape(shape);
    return SetShape<ChildExpr, decltype(lshape)>(~expr, ~lshape);
}

template <std::size_t... Dims,
    typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto ensure_shape(
    Tensor<ChildExpr, ValueType, Rank> const& expr)
{
    auto lshape = lazy_shape<Dims...>();
    return SetShape<ChildExpr, decltype(lshape)>(
        ~expr, ~lshape);
}

template <typename ValueType, std::size_t ChildRank, typename ChildExpr,
    typename LikeValueType, std::size_t NewRank, typename LikeExpr>
inline auto ensure_shape_like(
    Tensor<ChildExpr, ValueType, ChildRank> const& expr,
    Tensor<LikeExpr, LikeValueType, NewRank> const& like)
{
    auto lshape = shape_of(like);
    return SetShape<ChildExpr, decltype(lshape)>(
        ~expr, ~lshape);
}
}

#endif // SETSHAPEEXPR_HPP
