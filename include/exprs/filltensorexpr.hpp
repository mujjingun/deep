#ifndef FILLTENSOREXPR_HPP
#define FILLTENSOREXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

namespace internal {
    template <typename ValueType, int Value>
    struct FillTensorLambda {
        auto operator()(Queue&) const
        {
            return [](cl::sycl::handler&) {
                return [](auto) {
                    return ValueType(Value);
                };
            };
        }
    };
}

template <typename ValueType, int Value, std::size_t Rank, typename ShapeType>
struct FillTensor : Tensor<FillTensor<ValueType, Value, Rank, ShapeType>, ValueType, Rank> {
    using SShape = typename ShapeType::SShape;
    std::tuple<> m_args;

    ShapeType m_shape;

    FillTensor(ShapeType const& shape)
        : m_shape(~shape)
    {
    }

    auto build() const
    {
        internal::FillTensorLambda<ValueType, Value> lambda{};
        return make_pair(m_shape.get(), lambda);
    }

    std::string name() const
    {
        return std::string("FillTensor");
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add ShapeType
        return hash_combine(hash_str("FillTensor"),
            hash_type<ValueType>(), Value, Rank);
    }
};

template <typename ValueType, std::size_t Rank, typename ShapeType>
inline constexpr std::true_type is_zero_expr_impl(FillTensor<ValueType, 0, Rank, ShapeType> const&);
template <typename T>
inline constexpr std::false_type is_zero_expr_impl(T const&);
template <typename T>
inline constexpr bool is_zero_expr = decltype(is_zero_expr_impl(std::declval<T>()))::value;

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto zeros_like(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    auto lshape = shape_of(child);
    return FillTensor<ValueType, 0, Rank, decltype(lshape)>(~lshape);
}

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto ones_like(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    auto lshape = shape_of(child);
    return FillTensor<ValueType, 1, Rank, decltype(lshape)>(~lshape);
}

template <typename ValueType, std::size_t Rank>
inline auto zeros(Shape<Rank> shape)
{
    auto lshape = lazy_shape(shape);
    return FillTensor<ValueType, 0, Rank, decltype(lshape)>(~lshape);
}

template <typename ValueType, std::size_t Rank>
inline auto ones(Shape<Rank> shape)
{
    auto lshape = lazy_shape(shape);
    return FillTensor<ValueType, 1, Rank, decltype(lshape)>(~lshape);
}
}

#endif // FILLTENSOREXPR_HPP
