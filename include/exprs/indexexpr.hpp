#ifndef INDEXEXPR_HPP
#define INDEXEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

#include <iostream>

namespace dp {

inline constexpr std::size_t Last = std::numeric_limits<std::size_t>::max();

struct Range {
    std::size_t first = 0;
    std::size_t last = Last;
    std::ptrdiff_t stride = 1;
};

template <typename Child, typename ValueType>
inline constexpr void is_vector_impl(Tensor<Child, ValueType, 1> const&);
template <typename T>
using is_vector_impl_t = decltype(is_vector_impl(std::declval<T>()));
template <typename T>
inline constexpr bool is_vector = is_detected<is_vector_impl_t, T>::value;

template <typename Child, std::size_t SDim>
inline constexpr void is_dimexpr_impl(DimExpr<Child, SDim> const&);
template <typename T>
using is_dimexpr_impl_t = decltype(is_dimexpr_impl(std::declval<T>()));
template <typename T>
inline constexpr bool is_dimexpr = is_detected<is_dimexpr_impl_t, T>::value;

template <std::size_t Rank, typename Lambda>
inline constexpr void is_pair_impl(Pair<Rank, Lambda> const&);
template <typename T>
using is_pair_impl_t = decltype(is_pair_impl(std::declval<T>()));
template <typename T>
inline constexpr bool is_pair = is_detected<is_pair_impl_t, T>::value;

namespace internal {
    template <typename T>
    constexpr inline auto eval_dim(std::size_t dim, T const& index)
    {
        if constexpr (std::is_same_v<T, Range>) {
            D_ASSERT(index.stride != 0, "stride must not be zero");

            std::size_t first = index.first;
            if (Last - dim < first && first <= Last) {
                first = dim + first - Last - 1;
            }
            std::size_t last = index.last;
            if (Last - dim < last && last <= Last) {
                last = dim + last - Last - 1;
            }
            D_ASSERT(first < dim && last < dim, "first and last not in range");

            return Range{ first, last, index.stride };

        } else if constexpr (is_dimexpr<T>) {
            auto idx = index.get();

            D_ASSERT(idx < dim, "index not in range");

            return idx;
        } else if constexpr (is_vector<T>) {
            return index.get();
        }
    }

    template <std::size_t Rank, typename... IndexTypes>
    constexpr inline auto evaluate_indices(
        Shape<Rank> shape,
        std::tuple<IndexTypes...> const& indices)
    {
        return apply([](auto... pairs) {
            return std::make_tuple(
                eval_dim(std::get<0>(pairs), std::get<1>(pairs))...);
        },
            shape, indices);
    }

    template <typename T>
    constexpr inline auto calc_dim(T const& index)
    {
        if constexpr (std::is_same_v<T, Range>) {
            std::size_t first = index.first;
            std::size_t last = index.last;

            std::size_t stride{};
            if (index.stride > 0) {
                stride = index.stride;
            } else {
                std::swap(first, last);
                stride = -index.stride;
            }
            D_ASSERT(last >= first, "new size must be greater than 0");

            return (last - first) / stride + 1;

        } else {
            return filter_out_t{};
        }
    };

    template <typename T>
    constexpr inline auto filter_pairs(T index)
    {
        if constexpr (is_pair<T>) {
            return index;
        } else {
            return filter_out_t{};
        }
    };

    template <typename... IndexTypes>
    constexpr inline auto calc_indexed_shape(
        std::tuple<IndexTypes...> const& indices)
    {
        if constexpr ((... || is_pair<IndexTypes>)) {
            auto shapes = std::apply([](auto const&... indices) {
                return filter([](auto idx0, auto... idxs) {
                    D_ASSERT(((idx0.shape == idxs.shape) && ...), "Indexing arrays must have the same shape");
                    return std::make_tuple(idx0.shape, idxs.shape...);
                },
                    filter_pairs(indices)...);
            },
                indices);
            std::size_t count = std::get<0>(shapes)[0];
            return std::apply([&](auto const&... indices) {
                return filter([&](auto... dims) {
                    return Shape{ count, dims... };
                },
                    calc_dim(indices)...);
            },
                indices);
        } else {
            return std::apply([&](auto const&... indices) {
                return filter([](auto... dims) {
                    return Shape{ dims... };
                },
                    calc_dim(indices)...);
            },
                indices);
        }
    }

    template <std::size_t I>
    struct RangeIndexer {
        std::size_t first;
        std::ptrdiff_t stride;

        auto operator()(Queue&) const
        {
            return [first = first,
                       stride = stride](cl::sycl::handler&) {
                return [first, stride](auto idx) {
                    return idx[I] * stride + first;
                };
            };
        }
    };

    struct NormalIndexer {
        std::size_t index;

        auto operator()(Queue&) const
        {
            return [index = index](cl::sycl::handler&) {
                return [index](auto) {
                    return index;
                };
            };
        }
    };

    template <typename Lambda>
    struct ArrayIndexer {
        Lambda l;

        auto operator()(Queue& q) const
        {
            return [l = l(q)](cl::sycl::handler& cgh) {
                return [l = l(cgh)](auto idx) {
                    auto ret = l(Index{ idx[0] });
                    return ret;
                };
            };
        }
    };

    template <typename... IndexTypes>
    constexpr inline auto index_to_range_num
        = for_range_static<sizeof...(IndexTypes)>([](auto... j) {
              std::size_t i = 0;
              std::array<std::size_t, sizeof...(IndexTypes)> mapping{};
              auto func = [&](auto jc) {
                  constexpr auto j = decltype(jc)::value;
                  using IndexType = std::tuple_element_t<j, std::tuple<IndexTypes...>>;
                  if constexpr (std::is_same_v<IndexType, Range>) {
                      mapping[j] = i++;
                  }
              };
              (func(j), ...);
              return mapping;
          });

    template <typename... IndexTypes>
    inline auto indexer(std::tuple<IndexTypes...> const& indices)
    {
        return for_range_static<sizeof...(IndexTypes)>([&](auto... i) {
            auto func = [&](auto ic) {
                constexpr auto i = decltype(ic)::value;
                auto index = std::get<i>(indices);
                if constexpr (std::is_same_v<decltype(index), Range>) {
                    constexpr auto j = index_to_range_num<IndexTypes...>[i]
                        + (... || is_pair<IndexTypes>);
                    return RangeIndexer<j>{ index.first, index.stride };
                } else if constexpr (std::is_same_v<decltype(index), std::size_t>) {
                    return NormalIndexer{ index };
                } else {
                    return ArrayIndexer<decltype(index.lambda)>{ index.lambda };
                }
            };

            auto funcs = std::make_tuple(func(i)...);
            return [funcs](Queue& q) {
                auto l = [&](auto... funcs) {
                    return std::tuple(funcs(q)...);
                };
                return [funcs = std::apply(l, funcs)](cl::sycl::handler& cgh) {
                    auto l = [&](auto... funcs) {
                        return Hold{ funcs(cgh)... };
                    };
                    return [funcs = std::apply(l, funcs)](auto idx) {
                        return funcs.apply([&](auto... f) {
                            return Index{ f(idx)... };
                        });
                    };
                };
            };
        });
    };

    template <typename T>
    inline auto index_cast(T x)
    {
        if constexpr (std::is_integral_v<T>) {
            return lazy_dim(static_cast<std::size_t>(x));
        } else {
            return x;
        }
    };
}

// maps sparse tensor -> dense tensor
class IndexExprTag;
template <typename ChildExpr, std::size_t Rank, typename... IndexTypes>
struct IndexExpr : Tensor<IndexExpr<ChildExpr, Rank, IndexTypes...>,
                       typename ChildExpr::ValueType, Rank> {
    using Tag = IndexExprTag;
    static_assert(ChildExpr::rank == sizeof...(IndexTypes));

    std::tuple<ChildExpr> m_args;
    std::tuple<IndexTypes...> m_indices;

    explicit IndexExpr(
        ChildExpr const& expr,
        std::tuple<IndexTypes...> indices)
        : m_args(~expr)
        , m_indices(indices)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();

        auto indices = internal::evaluate_indices(a.shape, m_indices);
        Shape<Rank> shape = internal::calc_indexed_shape(indices);

        // i = sparse tensor's index
        auto indexer = internal::indexer(indices);
        auto lambda = [indexer, a = a.lambda](Queue& q) {
            return [indexer = indexer(q),
                       a = a(q)](cl::sycl::handler& cgh) {
                return [indexer = indexer(cgh),
                           a = a(cgh)](auto idx) {
                    return a(indexer(idx));
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "IndexExpr";
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add IndexTypes...
        return hash_combine(hash_str("IndexExpr"),
            ChildExpr::hash(), Rank);
    }
};

template <typename ChildExpr, typename ValueType, std::size_t ChildRank,
    typename... IndexTypes>
inline auto index(
    Tensor<ChildExpr, ValueType, ChildRank> const& expr,
    IndexTypes... indices)
{
    static_assert(ChildRank >= sizeof...(IndexTypes));
    constexpr std::size_t NRest = ChildRank - sizeof...(IndexTypes);

    if constexpr (NRest > 0) {
        return index(expr, indices..., Range{});
    } else {
        constexpr std::size_t Rank = (... || is_vector<IndexTypes>) //
            +(0 + ... + std::is_same_v<IndexTypes, Range>);
        return IndexExpr<ChildExpr, Rank,
            decltype(internal::index_cast(std::declval<IndexTypes>()))...>(
            ~expr, std::make_tuple(internal::index_cast(indices)...));
    }
}

template <typename Child, typename ValueType_, std::size_t Rank>
template <typename... IndexTypes>
inline auto Tensor<Child, ValueType_, Rank>::index(IndexTypes... indices) const
{
    return dp::index(*this, indices...);
}

namespace internal {
    struct FirstStridePair {
        std::size_t first;
        std::ptrdiff_t stride;
    };

    template <typename T>
    constexpr inline auto bp_hold(T const& index)
    {
        if constexpr (std::is_same_v<T, Range>) {
            return [first = index.first,
                       stride = index.stride](Queue&) {
                return [first, stride](cl::sycl::handler&) {
                    return FirstStridePair{ first, stride };
                };
            };
        } else if constexpr (std::is_same_v<T, std::size_t>) {
            return [index](Queue&) {
                return [index](cl::sycl::handler&) {
                    return index;
                };
            };
        } else if constexpr (is_pair<T>) {
            return [l = index.lambda](Queue& q) {
                return [l = l(q)](cl::sycl::handler& cgh) {
                    return l(cgh);
                };
            };
        }
    }

    // for non-array indices
    struct conn_check {
        template <typename T>
        constexpr auto operator()(std::size_t dim, T const& index) const
        {
            if constexpr (std::is_same_v<T, FirstStridePair>) {
                std::ptrdiff_t offset = dim - index.first;
                bool connected = ((index.stride > 0 && offset >= 0)
                                     || (index.stride < 0 && offset <= 0))
                    && (offset % index.stride == 0);
                return std::make_pair(
                    connected,
                    std::size_t(offset / index.stride));
            } else if constexpr (std::is_same_v<T, std::size_t>) {
                return std::make_pair(dim == index, filter_out_t{});
            }
        }
    };

    template <typename ValueType, typename Lambda, typename... Ts>
    constexpr inline auto bp_apply_index(
        ValueType& ret,
        Lambda const& a,
        Ts const&... pairs)
    {
        auto make_index = [&](auto... dims) {
            return Index{ dims... };
        };
        if ((... && pairs.first)) {
            ret += a(filter(make_index, pairs.second...));
        }
    }

    template <typename ValueType, typename Lambda, std::size_t Rank, typename... Ts, std::size_t... Is>
    constexpr inline auto bp_index(
        Lambda const& a,
        Index<Rank> const& idx,
        std::index_sequence<Is...>,
        Ts const&... indices)
    {
        auto ret = ValueType(0);
        conn_check check{};
        bp_apply_index(ret, a, check(idx[Is], indices)...);
        return ret;
    }

    template <typename ValueType, typename... IndexTypes>
    constexpr inline auto get_bp_indexer(
        std::tuple<IndexTypes...> const& indices)
    {
        // indexing without array
        auto hold = std::apply([](auto const&... indices) {
            return std::make_tuple(bp_hold(indices)...);
        },
            indices);

        return [hold](Queue& q) {
            auto l = [&](auto... funcs) {
                return std::tuple(funcs(q)...);
            };
            return [hold = std::apply(l, hold)](cl::sycl::handler& cgh) {
                auto l = [&](auto... funcs) {
                    return Hold{ funcs(cgh)... };
                };
                return [hold = std::apply(l, hold)](auto const& a, auto const& idx) {
                    return hold.apply([&](auto const&... indices) {
                        return bp_index<ValueType>(a, idx,
                            std::make_index_sequence<sizeof...(indices)>{},
                            indices...);
                    });
                };
            };
        };
    }

    // for array indices
    struct conn_check_array {
        std::size_t i;

        template <typename T>
        constexpr auto operator()(std::size_t dim, T const& index) const
        {
            if constexpr (std::is_same_v<T, FirstStridePair>) {
                std::ptrdiff_t offset = dim - index.first;
                bool connected = ((index.stride > 0 && offset >= 0)
                                     || (index.stride < 0 && offset <= 0))
                    && (offset % index.stride == 0);
                return std::make_pair(
                    connected,
                    std::size_t(offset / index.stride));
            } else if constexpr (std::is_same_v<T, std::size_t>) {
                return std::make_pair(dim == index, filter_out_t{});
            } else {
                return std::make_pair(dim == index(Index{ i }), filter_out_t{});
            }
        }
    };

    template <typename ValueType, typename Lambda, typename... Ts>
    constexpr inline auto bp_apply_index_array(
        ValueType& ret,
        Lambda const& a,
        std::size_t i,
        Ts const&... pairs)
    {
        auto make_index = [&](auto... dims) {
            return Index{ i, dims... };
        };
        if ((... && pairs.first)) {
            ret += a(filter(make_index, pairs.second...));
        }
    }

    template <typename ValueType, typename Lambda, std::size_t Rank, typename... Ts, std::size_t... Is>
    constexpr inline auto bp_index_array(
        Lambda const& a,
        Index<Rank> const& idx,
        std::size_t count,
        std::index_sequence<Is...>,
        Ts const&... indices)
    {
        auto ret = ValueType(0);
        for (std::size_t i = 0; i < count; ++i) {
            conn_check_array check{ i };
            bp_apply_index_array(ret, a, i, check(idx[Is], indices)...);
        }
        return ret;
    }

    template <typename ValueType, typename... IndexTypes>
    constexpr inline auto get_bp_indexer_array(
        std::tuple<IndexTypes...> const& indices)
    {
        // indexing with array
        auto shapes = std::apply([](auto const&... indices) {
            return filter([](auto idx0, auto... idxs) {
                D_ASSERT(((idx0.shape == idxs.shape) && ...), "Indexing arrays must have the same shape");
                return std::make_tuple(idx0.shape, idxs.shape...);
            },
                filter_pairs(indices)...);
        },
            indices);
        std::size_t count = std::get<0>(shapes)[0];

        auto hold = std::apply([](auto const&... indices) {
            return std::make_tuple(bp_hold(indices)...);
        },
            indices);

        return [count, hold](Queue& q) {
            auto l = [&](auto... funcs) {
                return std::tuple(funcs(q)...);
            };
            return [count, hold = std::apply(l, hold)](cl::sycl::handler& cgh) {
                auto l = [&](auto... funcs) {
                    return Hold{ funcs(cgh)... };
                };
                return [count, hold = std::apply(l, hold)](auto const& a, auto const& idx) {
                    return hold.apply([&](auto const&... indices) {
                        return bp_index_array<ValueType>(a, idx, count,
                            std::make_index_sequence<sizeof...(indices)>{},
                            indices...);
                    });
                };
            };
        };
    }
}

// maps dense tensor -> sparse tensor
template <typename ChildExpr, typename ShapeType, typename... IndexTypes>
struct IndexBackpropExpr : Tensor<IndexBackpropExpr<ChildExpr, ShapeType, IndexTypes...>,
                               typename ChildExpr::ValueType,
                               ShapeType::rank> {
    using SShape = typename ShapeType::SShape;
    std::tuple<ChildExpr> m_args;
    ShapeType m_shape;
    std::tuple<IndexTypes...> m_indices;

    static_assert(ShapeType::rank == sizeof...(IndexTypes));

    explicit IndexBackpropExpr(
        ChildExpr const& expr,
        ShapeType const& shape,
        std::tuple<IndexTypes...> const& indices)
        : m_args(~expr)
        , m_shape(~shape)
        , m_indices(indices)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();
        auto shape = m_shape.get();

        auto indices = internal::evaluate_indices(shape, m_indices);
        auto indexer = [&] {
            if constexpr (!(... || is_vector<IndexTypes>)) {
                return internal::get_bp_indexer<
                    typename ChildExpr::ValueType>(indices);
            } else {
                return internal::get_bp_indexer_array<
                    typename ChildExpr::ValueType>(indices);
            }
        }();

        auto lambda = [indexer, a = a.lambda](Queue& q) {
            return [indexer = indexer(q), a = a(q)](cl::sycl::handler& cgh) {
                return [indexer = indexer(cgh), a = a(cgh)](auto idx) {
                    return indexer(a, idx);
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "IndexBackpropExpr";
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add ShapeType and IndexTypes...
        return hash_combine(hash_str("IndexBackpropExpr"),
            ChildExpr::hash());
    }
};

template <typename ChildExpr, std::size_t ChildRank, typename ValueType,
    typename ShapeType, std::size_t Rank,
    typename... IndexTypes>
inline auto index_backprop(
    Tensor<ChildExpr, ValueType, ChildRank> const& expr,
    ShapeExpr<ShapeType, Rank> const& shape,
    std::tuple<IndexTypes...> indices)
{
    return IndexBackpropExpr<ChildExpr, ShapeType, IndexTypes...>(
        ~expr, ~shape, indices);
}
}

#endif // INDEXEXPR_HPP
