#ifndef TUPLEEXPR_HPP
#define TUPLEEXPR_HPP

#include "exprbase.hpp"

namespace dp {
template <typename... ElementTypes>
struct TupleExpr : ExprBase<TupleExpr<ElementTypes...>>,
                   std::tuple<LeafType<ElementTypes>...> {

    constexpr static std::size_t num_args = sizeof...(ElementTypes);

    explicit TupleExpr(LeafType<ElementTypes> const&... exprs)
        : std::tuple<LeafType<ElementTypes>...>((~exprs)...)
    {
    }

    template <std::size_t N>
    constexpr auto& arg()
    {
        return std::get<N>(*this);
    }

    template <std::size_t N>
    constexpr const auto& arg() const
    {
        return std::get<N>(*this);
    }

    constexpr TupleExpr const& operator~() const
    {
        return *this;
    }

    constexpr TupleExpr& operator~()
    {
        return *this;
    }

    auto run_impl(Queue& q)
    {
        auto eval = [&](auto... args) {
            return std::make_tuple(args.run(q)...);
        };
        return std::apply(eval, *this);
    }

    std::string name() const
    {
        return "TupleExpr";
    }
};

template <typename... ElementTypes>
inline auto make_tuple(ExprBase<ElementTypes> const&... exprs)
{
    return TupleExpr<ElementTypes...>(~exprs...);
}

template <typename... ElemTypes0, typename... ElemTypes1>
inline auto operator+(
    TupleExpr<ElemTypes0...> const& tup0,
    TupleExpr<ElemTypes1...> const& tup1)
{
    auto func = [](auto const&... pairs) {
        return dp::make_tuple((~std::get<0>(pairs) + ~std::get<1>(pairs))...);
    };
    return apply(func, tup0, tup1);
}
}

// specializations for std::tuple compatibility
namespace std {
template <std::size_t I, typename... T>
decltype(auto) get(dp::TupleExpr<T...>&& v)
{
    return std::get<I>(static_cast<std::tuple<T...>&&>(v));
}
template <std::size_t I, typename... T>
decltype(auto) get(dp::TupleExpr<T...>& v)
{
    return std::get<I>(static_cast<std::tuple<T...>&>(v));
}
template <std::size_t I, typename... T>
decltype(auto) get(dp::TupleExpr<T...> const& v)
{
    return std::get<I>(static_cast<std::tuple<T...> const&>(v));
}
template <typename... T>
struct tuple_size<dp::TupleExpr<T...>> : std::integral_constant<std::size_t, sizeof...(T)> {
};
}

#endif // TUPLEEXPR_HPP
