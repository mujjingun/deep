#ifndef CONSTTENSOREXPR_HPP
#define CONSTTENSOREXPR_HPP

#include "filltensorexpr.hpp"

namespace dp {

class VariableTensorTag;
template <typename ValueType, std::size_t Rank>
struct VariableTensor : Tensor<VariableTensor<ValueType, Rank>, ValueType, Rank> {
    using Tag = VariableTensorTag;

    std::tuple<> m_args;

    std::shared_ptr<Buffer<ValueType, Rank>> m_buf;

    template <typename Iterator>
    explicit VariableTensor(Shape<Rank> shape, Iterator begin)
        : m_buf(std::make_shared<Buffer<ValueType, Rank>>(shape, begin))
    {
    }

    auto build() const
    {
        auto shape = m_buf->shape();
        auto lambda = [this](Queue&) {
            return [this](cl::sycl::handler& cgh) {
                auto acc = m_buf->get_read_access(cgh);
                return [acc](auto idx) {
                    return acc[idx];
                };
            };
        };
        return make_pair(shape, lambda);
    }

    Buffer<ValueType, Rank>& buf() const
    {
        return *m_buf;
    }

    auto get_read_access(cl::sycl::handler& cgh) const
    {
        return m_buf->get_read_access(cgh);
    }
    auto get_write_access(cl::sycl::handler& cgh) const
    {
        return m_buf->get_write_access(cgh);
    }
    auto get_rw_access(cl::sycl::handler& cgh) const
    {
        return m_buf->get_rw_access(cgh);
    }

    auto get_read_access() const
    {
        return m_buf->get_read_access();
    }
    auto get_write_access() const
    {
        return m_buf->get_write_access();
    }
    auto get_rw_access() const
    {
        return m_buf->get_rw_access();
    }

    std::string name() const
    {
        return "VariableTensor";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("VariableTensor"), hash_type<ValueType>(), Rank);
    }
};

template <std::size_t Rank, typename Iterator>
inline auto var(Shape<Rank> shape, Iterator begin)
    -> VariableTensor<std::decay_t<decltype(*begin)>, Rank>
{
    using ValueType = std::decay_t<decltype(*begin)>;
    return VariableTensor<ValueType, Rank>(shape, begin);
}

template <typename ValueType, std::size_t Rank>
inline auto var(Shape<Rank> shape, std::initializer_list<ValueType> values)
{
    return var(shape, values.begin());
}

template <typename ValueType>
inline auto var(ValueType value)
{
    return VariableTensor<ValueType, 0>(Shape{}, &value);
}
}

#endif // CONSTTENSOREXPR_HPP
