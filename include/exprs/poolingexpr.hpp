#ifndef POOLINGEXPR_HPP
#define POOLINGEXPR_HPP

#include "elemwiseexpr.hpp"
#include "reduceexpr.hpp"
#include "reshapeexpr.hpp"
#include "tensorexpr.hpp"

#include <iostream>

namespace dp {

template <typename ValueType, typename ChildExpr>
inline auto max_pooling(
    Tensor<ChildExpr, ValueType, 4> const& expr,
    Shape<2> kernel_size)
{
    auto shape = shape_of(~expr);
    auto kernel = lazy_shape(kernel_size);
    auto M = nth_dim<1>(shape);
    auto K = nth_dim<0>(kernel);
    auto N = nth_dim<2>(shape);
    auto L = nth_dim<1>(kernel);
    auto new_shape = lazy_shape(nth_dim<0>(shape), M / K, K, N / L, L, nth_dim<3>(shape));
    return max<2, 4>(reshape(~expr, new_shape));
}
}

#endif // POOLINGEXPR_HPP
