#ifndef UPDATEEXPR_HPP
#define UPDATEEXPR_HPP

#include "setidexpr.hpp"
#include "tensorexpr.hpp"
#include "vartensorexpr.hpp"

namespace dp {

template <typename ChildExpr>
struct UpdateTensor : ExprBase<UpdateTensor<ChildExpr>> {
    constexpr static std::size_t num_args = 1;
    using ValueType = typename ChildExpr::ValueType;
    constexpr static std::size_t Rank = ChildExpr::rank;
    using IdType = ClIdType<Rank>;

    std::tuple<ChildExpr> m_args;

    VariableTensor<ValueType, Rank> m_var;

    UpdateTensor(
        VariableTensor<ValueType, Rank> const& var,
        ChildExpr const& expr)
        : m_args(~expr)
        , m_var(var)
    {
    }

    template <std::size_t N>
    constexpr auto& arg()
    {
        return std::get<N>(m_args);
    }

    template <std::size_t N>
    constexpr const auto& arg() const
    {
        return std::get<N>(m_args);
    }

    constexpr decltype(auto) operator~() const
    {
        return *this;
    }

    constexpr decltype(auto) operator~()
    {
        return *this;
    }

    auto run_impl(Queue& q) const
    {
        using namespace cl::sycl;

        auto a = this->arg0().get();

        D_ASSERT(a.shape == m_var.buf().shape(), "Shapes must be the same");

        auto command = a.lambda(q);

        q.submit([&, shape = a.shape](handler& cgh) {
            auto eval = command(cgh);
            auto acc = m_var.get_write_access(cgh);

            cgh.parallel_for<internal::Tag0<static_cast<std::int64_t>(hash())>>(
                to_range(shape),
                [acc, eval, shape](IdType id) {
                    Index<Rank> idx = to_index<Rank>(id, shape);
                    acc.m_acc[id] = eval(idx);
                });
        });

        return true;
    }

    std::string name() const
    {
        return "UpdateTensor";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("UpdateTensor"), ChildExpr::hash());
    }
};

template <typename ValueType, std::size_t Rank, typename VarExpr, typename UpdateExpr>
inline auto update(
    Tensor<VarExpr, ValueType, Rank> const& var,
    Tensor<UpdateExpr, ValueType, Rank> const& expr)
{
    if constexpr (std::is_same_v<typename VarExpr::Tag, VariableTensorTag>) {
        return UpdateTensor<UpdateExpr>(~var, ~expr);
    } else if constexpr (std::is_same_v<typename VarExpr::Tag, SetIdTensorTag>) {
        return update((~var).m_child, expr);
    }
}
}

#endif // UPDATEEXPR_HPP
