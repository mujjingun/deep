#ifndef ELEMWISEEXPR_HPP
#define ELEMWISEEXPR_HPP

#include "adddimexpr.hpp"
#include "filltensorexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

class ElemwiseUnaryTensorTag;
template <typename Operator_, typename Expr>
struct ElemwiseUnaryTensor : Tensor<ElemwiseUnaryTensor<Operator_, Expr>,
                                 decltype(Operator_{}(typename Expr::ValueType{})),
                                 Expr::rank> {
    using Tag = ElemwiseUnaryTensorTag;
    using Operator = Operator_;
    using SShape = typename Expr::SShape;
    std::tuple<Expr> m_args;

    explicit ElemwiseUnaryTensor(Expr const& expr)
        : m_args(~expr)
    {
    }

    auto build() const
    {
        auto [shape, a] = this->arg0().get();

        auto lambda = [a = a](Queue& q) {
            return [a = a(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh)](auto idx) {
                    return Operator{}(a(idx));
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("ElemwiseUnaryTensor");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ElemwiseUnaryTensor"),
            Operator::hash(), Expr::hash());
    }
};

template <typename Operator,
    typename ValueType, std::size_t Rank,
    typename Expr>
inline auto elem_unary(
    Tensor<Expr, ValueType, Rank> const& expr)
{
    return ElemwiseUnaryTensor<Operator, Expr>(~expr);
}

namespace internal {
    template <typename ValueType>
    struct functor_cast {
        template <typename T>
        auto operator()(T x) const { return static_cast<ValueType>(x); }
        static constexpr std::uint64_t hash()
        {
            return hash_combine(hash_str("cast"), hash_type<ValueType>());
        }
    };

    template <typename ValueType>
    inline constexpr std::true_type is_cast_impl(functor_cast<ValueType> const&);
    template <typename T>
    inline constexpr std::false_type is_cast_impl(T const&);
    template <typename T>
    inline constexpr bool is_cast = decltype(is_cast_impl(std::declval<T>()))::value;
}
template <typename NewValueType,
    typename ValueType, std::size_t Rank,
    typename Expr>
inline auto cast(
    Tensor<Expr, ValueType, Rank> const& expr)
{
    return elem_unary<internal::functor_cast<NewValueType>>(expr);
}

#define D_DECL_UNARYOP(name, func)                                            \
    namespace internal {                                                      \
        struct functor_##name {                                               \
            template <typename T>                                             \
            auto operator()(T x) const { return func(x); }                    \
            static constexpr std::uint64_t hash() { return hash_str(#name); } \
        };                                                                    \
    }                                                                         \
    template <typename ValueType, std::size_t Rank, typename Expr>            \
    inline auto elem_##name(                                                  \
        Tensor<Expr, ValueType, Rank> const& expr)                            \
    {                                                                         \
        return elem_unary<internal::functor_##name>(expr);                    \
    }
D_DECL_UNARYOP(negate, std::negate<>{})

#define D_DECL_SYCL_UNARYOP(name) D_DECL_UNARYOP(name, cl::sycl::name);
// same precision on both host & device
D_DECL_SYCL_UNARYOP(acos)
D_DECL_SYCL_UNARYOP(acosh)
D_DECL_SYCL_UNARYOP(acospi)
D_DECL_SYCL_UNARYOP(asin)
D_DECL_SYCL_UNARYOP(asinh)
D_DECL_SYCL_UNARYOP(asinpi)
D_DECL_SYCL_UNARYOP(atan)
D_DECL_SYCL_UNARYOP(atanh)
D_DECL_SYCL_UNARYOP(atanpi)
D_DECL_SYCL_UNARYOP(cbrt)
D_DECL_SYCL_UNARYOP(ceil)
D_DECL_SYCL_UNARYOP(cos)
D_DECL_SYCL_UNARYOP(cosh)
D_DECL_SYCL_UNARYOP(erfc)
D_DECL_SYCL_UNARYOP(erf)
D_DECL_SYCL_UNARYOP(exp)
D_DECL_SYCL_UNARYOP(exp2)
D_DECL_SYCL_UNARYOP(exp10)
D_DECL_SYCL_UNARYOP(expm1)
D_DECL_SYCL_UNARYOP(fabs)
D_DECL_SYCL_UNARYOP(floor)
D_DECL_SYCL_UNARYOP(lgamma)
D_DECL_SYCL_UNARYOP(log)
D_DECL_SYCL_UNARYOP(log2)
D_DECL_SYCL_UNARYOP(log10)
D_DECL_SYCL_UNARYOP(log1p)
D_DECL_SYCL_UNARYOP(logb)
D_DECL_SYCL_UNARYOP(rint)
D_DECL_SYCL_UNARYOP(round)
D_DECL_SYCL_UNARYOP(rsqrt)
D_DECL_SYCL_UNARYOP(sin)
D_DECL_SYCL_UNARYOP(sinh)
D_DECL_SYCL_UNARYOP(sinpi)
D_DECL_SYCL_UNARYOP(sqrt)
D_DECL_SYCL_UNARYOP(tan)
D_DECL_SYCL_UNARYOP(tanh)
D_DECL_SYCL_UNARYOP(tanpi)
D_DECL_SYCL_UNARYOP(tgamma)
D_DECL_SYCL_UNARYOP(trunc)
#undef D_DECL_SYCL_UNARYOP
#undef D_DECL_UNARYOP

template <typename ValueType, std::size_t Rank, typename Expr>
inline auto operator-(
    Tensor<Expr, ValueType, Rank> const& expr)
{
    if constexpr (is_zero_expr<Expr>) {
        return zeros_like(~expr);
    } else {
        return elem_unary<internal::functor_negate>(expr);
    }
}

namespace internal {
    template <typename SShape0, typename SShape1, std::size_t... I>
    inline constexpr auto broadcast_shapes_impl(std::index_sequence<I...>)
    {
        constexpr auto shape0 = SShape0::to_shape();
        constexpr auto shape1 = SShape1::to_shape();

        if constexpr (SShape0::Rank != SShape1::Rank) {
            display<SShape0, SShape1>{};
        }
        static_assert(SShape0::Rank == SShape1::Rank);

        constexpr bool valid = (...
            && (shape0[I] == shape1[I]
                   || shape0[I] == 1 || shape1[I] == 1
                   || shape0[I] == UnknownDim || shape1[I] == UnknownDim));
        if constexpr (!valid) {
            display<SShape0, SShape1>{};
        }
        static_assert(valid, "Shapes are not broadcastable");

        constexpr auto getdim = [=](auto i) {
            if (shape0[i] == 1) {
                return shape1[i];
            }
            if (shape1[i] == 1) {
                return shape0[i];
            }
            if (shape0[i] == UnknownDim) {
                return shape1[i];
            }
            if (shape1[i] == UnknownDim) {
                return shape0[i];
            }
            return shape0[i];
        };
        return StaticShape<(getdim(I))...>{};
    }

    template <typename... SShapes>
    struct broadcast_shapes;
    template <typename SShape0, typename SShape1>
    struct broadcast_shapes<SShape0, SShape1> {
        using type = decltype(broadcast_shapes_impl<SShape0, SShape1>(std::make_index_sequence<SShape0::Rank>{}));
    };
    template <typename SShape0, typename SShape1, typename SShape2, typename... SShapes>
    struct broadcast_shapes<SShape0, SShape1, SShape2, SShapes...> {
        using type = typename broadcast_shapes<typename broadcast_shapes<SShape0, SShape1>::type, SShape2, SShapes...>::type;
    };

    template <typename... SShapes>
    using broadcast_shapes_t = typename broadcast_shapes<SShapes...>::type;
}

class ElemwiseTensorTag;
template <typename Operator_, typename LhsExpr, typename RhsExpr>
struct ElemwiseTensor : Tensor<ElemwiseTensor<Operator_, LhsExpr, RhsExpr>,
                            decltype(Operator_{}(typename LhsExpr::ValueType{}, typename RhsExpr::ValueType{})),
                            LhsExpr::rank> {
    using Tag = ElemwiseTensorTag;
    using Operator = Operator_;
    using SShape = internal::broadcast_shapes_t<typename LhsExpr::SShape, typename RhsExpr::SShape>;
    std::tuple<LhsExpr, RhsExpr> m_args;

    static constexpr auto a_sshape = LhsExpr::SShape::to_shape();
    static constexpr auto b_sshape = RhsExpr::SShape::to_shape();

    explicit ElemwiseTensor(LhsExpr const& lhs, RhsExpr const& rhs)
        : m_args(~lhs, ~rhs)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();
        auto b = this->arg1().get();

        auto shape = for_range<LhsExpr::rank>([&](auto... i) {
            D_ASSERT((... && (a_sshape[i] == 1 || b_sshape[i] == 1 || a.shape[i] == b.shape[i])),
                "Shapes are not compatible");

            return Shape{ std::max(a.shape[i], b.shape[i])... };
        });

        auto lambda = [a = a.lambda, b = b.lambda](Queue& q) {
            return [a = a(q), b = b(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh), b = b(cgh)](auto idx) {
                    auto aidx = for_range<LhsExpr::rank>([&](auto... i) {
                        return Index{ (a_sshape[i] == 1 ? 0 : idx[i])... };
                    });
                    auto bidx = for_range<LhsExpr::rank>([&](auto... i) {
                        return Index{ (b_sshape[i] == 1 ? 0 : idx[i])... };
                    });
                    return Operator{}(a(aidx), b(bidx));
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("ElemwiseTensor");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ElemwiseTensor"),
            Operator::hash(), LhsExpr::hash(), RhsExpr::hash());
    }
};

template <std::size_t NewRank,
    typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto add_dim_until(
    Tensor<ChildExpr, ValueType, Rank> const& x)
{
    static_assert(NewRank >= Rank);
    if constexpr (NewRank == Rank) {
        return ~x;
    } else {
        return add_dim_until<NewRank>(add_dim<0>(~x));
    }
}

template <typename Operator,
    typename LhsValueType, std::size_t LhsRank, typename LhsExpr,
    typename RhsValueType, std::size_t RhsRank, typename RhsExpr>
inline auto elem_binary(
    Tensor<LhsExpr, LhsValueType, LhsRank> const& lhs,
    Tensor<RhsExpr, RhsValueType, RhsRank> const& rhs)
{
    constexpr std::size_t NewRank = std::max(LhsRank, RhsRank);
    auto a = add_dim_until<NewRank>(~lhs);
    auto b = add_dim_until<NewRank>(~rhs);
    return ElemwiseTensor<Operator,
        LeafType<decltype(a)>,
        LeafType<decltype(b)>>(~a, ~b);
}

#define D_DECL_BINOP(name, func)                                              \
    namespace internal {                                                      \
        struct functor_##name {                                               \
            template <typename A, typename B>                                 \
            auto operator()(A a, B b) const { return func(a, b); }            \
            static constexpr std::uint64_t hash() { return hash_str(#name); } \
        };                                                                    \
    }                                                                         \
    template <typename ValueType,                                             \
        std::size_t LhsRank, typename LhsExpr,                                \
        std::size_t RhsRank, typename RhsExpr>                                \
    inline auto elem_##name(                                                  \
        Tensor<LhsExpr, ValueType, LhsRank> const& lhs,                       \
        Tensor<RhsExpr, ValueType, RhsRank> const& rhs)                       \
    {                                                                         \
        return elem_binary<internal::functor_##name>(lhs, rhs);               \
    }
D_DECL_BINOP(plus, std::plus<>{})
D_DECL_BINOP(minus, std::minus<>{})
D_DECL_BINOP(multiplies, std::multiplies<>{})
D_DECL_BINOP(divides, std::divides<>{})

#define D_DECL_COMP(op, name)                                         \
    D_DECL_BINOP(name, std::name<>{})                                 \
    template <                                                        \
        typename LhsValueType, std::size_t LhsRank, typename LhsExpr, \
        typename RhsValueType, std::size_t RhsRank, typename RhsExpr> \
    inline auto operator op(                                          \
        Tensor<LhsExpr, LhsValueType, LhsRank> const& lhs,            \
        Tensor<RhsExpr, RhsValueType, RhsRank> const& rhs)            \
    {                                                                 \
        return elem_binary<internal::functor_##name>(lhs, rhs);       \
    }
D_DECL_COMP(==, equal_to)
D_DECL_COMP(!=, not_equal_to)
D_DECL_COMP(>, greater)
D_DECL_COMP(<, less)
D_DECL_COMP(>=, greater_equal)
D_DECL_COMP(<=, less_equal)
#undef D_DECL_COMP

#define D_DECL_SYCL_BINOP(name) D_DECL_BINOP(name, cl::sycl::name)
// same precision on both host & device
D_DECL_SYCL_BINOP(atan2)
D_DECL_SYCL_BINOP(atan2pi)
D_DECL_SYCL_BINOP(copysign)
D_DECL_SYCL_BINOP(fdim)
D_DECL_SYCL_BINOP(fmax)
D_DECL_SYCL_BINOP(fmin)
D_DECL_SYCL_BINOP(fmod)
D_DECL_SYCL_BINOP(frexp)
D_DECL_SYCL_BINOP(hypot)
D_DECL_SYCL_BINOP(maxmag)
D_DECL_SYCL_BINOP(minmag)
D_DECL_SYCL_BINOP(nextafter)
D_DECL_SYCL_BINOP(pow)
D_DECL_SYCL_BINOP(powr)
D_DECL_SYCL_BINOP(remainder)
#undef D_DECL_SYCL_BINOP

// implementation-defined precision
D_DECL_BINOP(n_divide, cl::sycl::native::divide)
D_DECL_BINOP(n_powr, cl::sycl::native::powr)
#undef D_DECL_BINOP

template <typename ValueType,
    std::size_t LhsRank, typename LhsExpr,
    std::size_t RhsRank, typename RhsExpr>
inline auto operator+(
    Tensor<LhsExpr, ValueType, LhsRank> const& lhs,
    Tensor<RhsExpr, ValueType, RhsRank> const& rhs)
{
    if constexpr (is_zero_expr<RhsExpr>) {
        return ~lhs;
    } else if constexpr (is_zero_expr<LhsExpr>) {
        return ~rhs;
    } else {
        return elem_binary<internal::functor_plus>(lhs, rhs);
    }
}

template <typename ValueType,
    std::size_t LhsRank, typename LhsExpr,
    std::size_t RhsRank, typename RhsExpr>
inline auto operator-(
    Tensor<LhsExpr, ValueType, LhsRank> const& lhs,
    Tensor<RhsExpr, ValueType, RhsRank> const& rhs)
{
    if constexpr (is_zero_expr<RhsExpr>) {
        return ~lhs;
    } else if constexpr (is_zero_expr<LhsExpr>) {
        return -(~rhs);
    } else {
        return elem_binary<internal::functor_minus>(lhs, rhs);
    }
}

/// element-wise multiplication
template <typename ValueType,
    std::size_t LhsRank, typename LhsExpr,
    std::size_t RhsRank, typename RhsExpr>
inline auto elem_mul(
    Tensor<LhsExpr, ValueType, LhsRank> const& lhs,
    Tensor<RhsExpr, ValueType, RhsRank> const& rhs)
{
    return elem_binary<internal::functor_multiplies>(lhs, rhs);
}

template <typename ValueType,
    std::size_t LhsRank, typename LhsExpr,
    std::size_t RhsRank, typename RhsExpr>
inline auto elem_div(
    Tensor<LhsExpr, ValueType, LhsRank> const& lhs,
    Tensor<RhsExpr, ValueType, RhsRank> const& rhs)
{
    return elem_binary<internal::functor_divides>(lhs, rhs);
}

/// tensor-scalar multiplication
template <typename ValueType, std::size_t Rank,
    typename LhsExpr, typename RhsExpr>
inline auto operator*(
    Tensor<LhsExpr, ValueType, Rank> const& lhs,
    Tensor<RhsExpr, ValueType, 0> const& rhs)
{
    return elem_mul(lhs, rhs);
}

template <typename ValueType, std::size_t Rank,
    typename LhsExpr, typename RhsExpr>
inline auto operator*(
    Tensor<LhsExpr, ValueType, 0> const& lhs,
    Tensor<RhsExpr, ValueType, Rank> const& rhs)
    -> std::enable_if_t<(Rank > 0), decltype(elem_mul(lhs, rhs))>
{
    return elem_mul(lhs, rhs);
}

/// divide by scalar
template <typename ValueType, std::size_t Rank,
    typename LhsExpr, typename RhsExpr>
inline auto operator/(
    Tensor<LhsExpr, ValueType, Rank> const& lhs,
    Tensor<RhsExpr, ValueType, 0> const& rhs)
{
    return elem_div(lhs, rhs);
}

template <typename ValueType, std::size_t Rank,
    typename LhsExpr, typename RhsExpr>
inline auto operator/(
    Tensor<LhsExpr, ValueType, 0> const& lhs,
    Tensor<RhsExpr, ValueType, Rank> const& rhs)
    -> std::enable_if_t<(Rank > 0), decltype(elem_div(lhs, rhs))>
{
    return elem_div(lhs, rhs);
}

class ElemwiseTernaryTensorTag;
template <typename Operator_, typename Expr0, typename Expr1, typename Expr2>
struct ElemwiseTernaryTensor : Tensor<ElemwiseTernaryTensor<Operator_, Expr0, Expr1, Expr2>,
                                   decltype(Operator_{}(typename Expr0::ValueType{}, typename Expr1::ValueType{}, typename Expr2::ValueType{})),
                                   Expr0::rank> {
    using Tag = ElemwiseTernaryTensorTag;
    using Operator = Operator_;
    using SShape = internal::broadcast_shapes_t<typename Expr0::SShape, typename Expr1::SShape, typename Expr2::SShape>;
    std::tuple<Expr0, Expr1, Expr2> m_args;

    static constexpr auto sshape0 = Expr0::SShape::to_shape();
    static constexpr auto sshape1 = Expr1::SShape::to_shape();
    static constexpr auto sshape2 = Expr2::SShape::to_shape();

    explicit ElemwiseTernaryTensor(
        Expr0 const& expr0,
        Expr1 const& expr1,
        Expr2 const& expr2)
        : m_args(~expr0, ~expr1, ~expr2)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();
        auto b = this->arg1().get();
        auto c = this->arg2().get();

        auto shape = for_range<Expr0::rank>([&](auto... i) {
            D_ASSERT((... && (sshape0[i] == 1 || sshape1[i] == 1 || sshape2[i] == 1 || (a.shape[i] == b.shape[i] && a.shape[i] == c.shape[i]))),
                "Shapes are not compatible");

            return Shape{ std::max({ a.shape[i], b.shape[i], c.shape[i] })... };
        });

        auto lambda = [a = a.lambda, b = b.lambda, c = c.lambda](Queue& q) {
            return [a = a(q), b = b(q), c = c(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh), b = b(cgh), c = c(cgh)](auto idx) {
                    auto aidx = for_range<Expr0::rank>([&](auto... i) {
                        return Index{ (sshape0[i] == 1 ? 0 : idx[i])... };
                    });
                    auto bidx = for_range<Expr1::rank>([&](auto... i) {
                        return Index{ (sshape1[i] == 1 ? 0 : idx[i])... };
                    });
                    auto cidx = for_range<Expr2::rank>([&](auto... i) {
                        return Index{ (sshape2[i] == 1 ? 0 : idx[i])... };
                    });
                    return Operator{}(a(aidx), b(bidx), c(cidx));
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("ElemwiseTernaryTensor");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ElemwiseTernaryTensor"),
            Operator::hash(), Expr0::hash(), Expr1::hash(), Expr2::hash());
    }
};

template <typename Operator,
    std::size_t Rank0, typename ValueType0, typename Expr0,
    std::size_t Rank1, typename ValueType1, typename Expr1,
    std::size_t Rank2, typename ValueType2, typename Expr2>
inline auto elem_ternary(
    Tensor<Expr0, ValueType0, Rank0> const& expr0,
    Tensor<Expr1, ValueType1, Rank1> const& expr1,
    Tensor<Expr2, ValueType2, Rank2> const& expr2)
{
    constexpr std::size_t NewRank = std::max({ Rank0, Rank1, Rank2 });
    auto arg0 = add_dim_until<NewRank>(~expr0);
    auto arg1 = add_dim_until<NewRank>(~expr1);
    auto arg2 = add_dim_until<NewRank>(~expr2);
    return ElemwiseTernaryTensor<Operator,
        LeafType<decltype(arg0)>,
        LeafType<decltype(arg1)>,
        LeafType<decltype(arg2)>>(~arg0, ~arg1, ~arg2);
}

namespace internal {
    struct functor_select {
        template <typename T>
        auto operator()(bool cond, T yes, T no) const
        {
            return cond ? yes : no;
        }
        static constexpr std::uint64_t hash() { return hash_str("select"); }
    };
}

template <typename ValueType,
    std::size_t Rank0, typename Expr0,
    std::size_t Rank1, typename Expr1,
    std::size_t Rank2, typename Expr2>
inline auto select(
    Tensor<Expr0, bool, Rank0> const& cond,
    Tensor<Expr1, ValueType, Rank1> const& yes,
    Tensor<Expr2, ValueType, Rank2> const& no)
{
    return elem_ternary<internal::functor_select>(cond, yes, no);
}
}

#endif // ELEMWISEEXPR_HPP
