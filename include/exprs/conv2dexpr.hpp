#ifndef CONV2DEXPR_HPP
#define CONV2DEXPR_HPP

#include "tensorexpr.hpp"

namespace dp {

/**
 *  Input tensor of shape [batch, in_height, in_width, in_channels]
 *  Kernel tensor of shape [filter_height, filter_width, in_channels, out_channels]
 */
class Conv2DTag;
template <typename InputExpr, typename KernelExpr>
struct Conv2D : Tensor<Conv2D<InputExpr, KernelExpr>,
                    typename InputExpr::ValueType, 4> {
    using Tag = Conv2DTag;
    static constexpr bool do_caching = true;

    std::tuple<InputExpr, KernelExpr> m_args;

    using ValueType = typename InputExpr::ValueType;

    explicit Conv2D(InputExpr const& input, KernelExpr const& kernel)
        : m_args(~input, ~kernel)
    {
    }

    auto build() const
    {
        auto [input_shape, input] = this->arg0().get();
        auto [kernel_shape, kernel] = this->arg1().get();

        D_ASSERT(input_shape[3] == kernel_shape[2], "Number of input channels must match");
        D_ASSERT(input_shape[1] >= kernel_shape[0], "Height of input must be equal or larger than the kernel's");
        D_ASSERT(input_shape[2] >= kernel_shape[1], "Width of input must be equal or larger than the kernel's");

        Shape<4> shape;
        shape[0] = input_shape[0]; // batch
        shape[1] = input_shape[1] - kernel_shape[0] + 1; // out_height
        shape[2] = input_shape[2] - kernel_shape[1] + 1; // out_width
        shape[3] = kernel_shape[3]; // out_channels

        auto lambda = [in_channels = input_shape[3],
                          kernel_height = kernel_shape[0],
                          kernel_width = kernel_shape[1],
                          input = input, kernel = kernel](Queue& q) {
            return [=, input = input(q),
                       kernel = kernel(q)](cl::sycl::handler& cgh) {
                return [in_channels,
                           kernel_height,
                           kernel_width,
                           input = input(cgh),
                           kernel = kernel(cgh)](auto idx) {
                    std::size_t batch = idx[0];
                    std::size_t i = idx[1], j = idx[2]; // height, width
                    std::size_t o = idx[3]; // out_channel
                    ValueType ret = ValueType(0);
                    for (std::size_t c = 0; c < in_channels; ++c) {
                        for (std::size_t h = 0; h < kernel_height; ++h) {
                            for (std::size_t w = 0; w < kernel_width; ++w) {
                                ret += input(Index(batch, i + h, j + w, c)) * kernel(Index(h, w, c, o));
                            }
                        }
                    }
                    return ret;
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("Conv2D");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("Conv2D"), InputExpr::hash(), KernelExpr::hash());
    }
};

template <typename ValueType, typename InputExpr, typename KernelExpr>
inline auto conv2d(
    Tensor<InputExpr, ValueType, 4> const& input,
    Tensor<KernelExpr, ValueType, 4> const& kernel)
{
    return Conv2D<InputExpr, KernelExpr>(~input, ~kernel);
}

/**
 *  Input tensor of shape [batch, in_height, in_width, in_channels]
 *  Kernel tensor of shape [filter_height, filter_width, in_channels, out_channels]
 */
template <typename InputExpr, typename DOutExpr>
struct Conv2DBackpropFilter : Tensor<Conv2DBackpropFilter<InputExpr, DOutExpr>,
                                  typename DOutExpr::ValueType, 4> {
    static constexpr bool do_caching = true;

    std::tuple<InputExpr, DOutExpr> m_args;

    using ValueType = typename DOutExpr::ValueType;

    explicit Conv2DBackpropFilter(InputExpr const& input, DOutExpr const& dout)
        : m_args(~input, ~dout)
    {
    }

    auto build() const
    {
        auto input = this->arg0().get();
        auto dout = this->arg1().get();

        D_ASSERT(input.shape[0] == dout.shape[0], "Number of batches must match");
        D_ASSERT(input.shape[1] >= dout.shape[1], "Height of input must be equal or larger than the output's");
        D_ASSERT(input.shape[2] >= dout.shape[2], "Width of input must be equal or larger than the output's");

        Shape<4> shape;
        shape[0] = input.shape[1] - dout.shape[1] + 1; // kernel_height
        shape[1] = input.shape[2] - dout.shape[2] + 1; // kernel_width
        shape[2] = input.shape[3]; // in_channels
        shape[3] = dout.shape[3]; // out_channels

        auto lambda = [batch = input.shape[0],
                          out_height = dout.shape[1],
                          out_width = dout.shape[2],
                          input = input.lambda, dout = dout.lambda](Queue& q) {
            return [=, input = input(q),
                       dout = dout(q)](cl::sycl::handler& cgh) {
                return [batch,
                           out_height,
                           out_width,
                           input = input(cgh),
                           dout = dout(cgh)](auto idx) {
                    std::size_t H = idx[0], W = idx[1];
                    std::size_t I = idx[2], O = idx[3];
                    ValueType ret = ValueType(0);
                    for (std::size_t b = 0; b < batch; ++b) {
                        for (std::size_t h = 0; h < out_height; ++h) {
                            for (std::size_t w = 0; w < out_width; ++w) {
                                ret += dout(Index(b, h, w, O)) * input(Index(b, h + H, w + W, I));
                            }
                        }
                    }
                    return ret;
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("Conv2DBackpropFilter");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("Conv2DBackpropFilter"), InputExpr::hash(), DOutExpr::hash());
    }
};

template <typename ValueType, typename InputExpr, typename DOutExpr>
inline auto conv2d_backprop_filter(
    Tensor<InputExpr, ValueType, 4> const& input,
    Tensor<DOutExpr, ValueType, 4> const& dout)
{
    return Conv2DBackpropFilter<InputExpr, DOutExpr>(~input, ~dout);
}

/**
 *  Input tensor of shape [batch, in_height, in_width, in_channels]
 *  Kernel tensor of shape [filter_height, filter_width, in_channels, out_channels]
 */
template <typename KernelExpr, typename DOutExpr>
struct Conv2DBackpropInput : Tensor<Conv2DBackpropInput<KernelExpr, DOutExpr>,
                                 typename DOutExpr::ValueType, 4> {
    static constexpr bool do_caching = true;

    std::tuple<KernelExpr, DOutExpr> m_args;

    using ValueType = typename DOutExpr::ValueType;

    explicit Conv2DBackpropInput(KernelExpr const& kernel, DOutExpr const& dout)
        : m_args(~kernel, ~dout)
    {
    }

    auto build() const
    {
        auto kernel = this->arg0().get();
        auto dout = this->arg1().get();

        D_ASSERT(kernel.shape[3] == dout.shape[3], "Number of output channels must match");

        Shape<4> shape;
        shape[0] = dout.shape[0]; // batch
        shape[1] = kernel.shape[0] + dout.shape[1] - 1; // in_height
        shape[2] = kernel.shape[1] + dout.shape[2] - 1; // in_width
        shape[3] = kernel.shape[2]; // in_channels

        auto lambda = [out_height = dout.shape[1],
                          out_width = dout.shape[2],
                          out_channels = dout.shape[3],
                          kernel_height = kernel.shape[0],
                          kernel_width = kernel.shape[1],
                          kernel = kernel.lambda, dout = dout.lambda](Queue& q) {
            return [=, kernel = kernel(q),
                       dout = dout(q)](cl::sycl::handler& cgh) {
                return [out_height,
                           out_width,
                           out_channels,
                           kernel_height,
                           kernel_width,
                           kernel = kernel(cgh),
                           dout = dout(cgh)](auto idx) {
                    std::size_t B = idx[0];
                    std::size_t H = idx[1], W = idx[2];
                    std::size_t I = idx[3];
                    ValueType ret = ValueType(0);
                    for (std::size_t d = 0; d < out_channels; ++d) {
                        for (std::size_t h = 0; h < kernel_height; ++h) {
                            for (std::size_t w = 0; w < kernel_width; ++w) {
                                if (H < h || H - h >= out_height
                                    || W < w || W - w >= out_width) {
                                    continue;
                                }
                                ret += dout(Index(B, H - h, W - w, d)) * kernel(Index(h, w, I, d));
                            }
                        }
                    }
                    return ret;
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("Conv2DBackpropInput");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("Conv2DBackpropInput"), KernelExpr::hash(), DOutExpr::hash());
    }
};

template <typename ValueType, typename KernelExpr, typename DOutExpr>
inline auto conv2d_backprop_input(
    Tensor<KernelExpr, ValueType, 4> const& kernel,
    Tensor<DOutExpr, ValueType, 4> const& dout)
{
    return Conv2DBackpropInput<KernelExpr, DOutExpr>(~kernel, ~dout);
}
}

#endif // CONV2DEXPR_HPP
