#ifndef ARANGEEXPR_HPP
#define ARANGEEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

template <typename ValueType, typename DimType>
struct ArangeExpr : Tensor<ArangeExpr<ValueType, DimType>,
                        ValueType, 1> {
    using SShape = StaticShape<DimType::SDim>;
    std::tuple<> m_args;

    DimType m_size;

    explicit ArangeExpr(DimType const& size)
        : m_size(size)
    {
    }

    auto build() const
    {
        auto size = m_size.get();
        Shape<1> shape{ size };

        auto lambda = [](Queue&) {
            return [](cl::sycl::handler&) {
                return [](auto idx) {
                    return ValueType(idx[0]);
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "ArangeExpr";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ArangeExpr"), hash_type<ValueType>());
    }
};

template <typename ValueType, typename DimType, std::size_t SDim>
inline auto arange(DimExpr<DimType, SDim> const& size)
{
    return ArangeExpr<ValueType, DimType>(~size);
}

template <typename ValueType>
inline auto arange(std::size_t size)
{
    auto lsize = lazy_dim(size);
    return arange<ValueType>(lsize);
}
}

#endif // ARANGEEXPR_HPP
