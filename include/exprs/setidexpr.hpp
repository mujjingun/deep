#ifndef SETIDEXPR_HPP
#define SETIDEXPR_HPP

#include "tensorexpr.hpp"

namespace dp {

class SetIdTensorTag;
template <typename ChildExpr, typename Id_>
struct SetIdTensor : Tensor<SetIdTensor<ChildExpr, Id_>,
                         typename ChildExpr::ValueType,
                         ChildExpr::rank> {
    using SShape = typename ChildExpr::SShape;
    using Id = Id_;
    using Tag = SetIdTensorTag;

    std::tuple<> m_args;

    ChildExpr m_child;

    SetIdTensor(ChildExpr const& child)
        : m_child(~child)
    {
    }

    auto build() const
    {
        return m_child.get();
    }

    std::string name() const
    {
        return std::string("SetIdTensor");
    }

    constexpr static std::uint64_t hash()
    {
        return ChildExpr::hash();
    }
};

template <typename Id, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto set_id(Tensor<ChildExpr, ValueType, Rank> const& expr)
{
    return SetIdTensor<ChildExpr, Id>(~expr);
}
}

#endif // SETIDEXPR_HPP
