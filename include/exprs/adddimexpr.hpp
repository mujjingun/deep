#ifndef ADDDIMEXPR_HPP
#define ADDDIMEXPR_HPP

#include "tensorexpr.hpp"

namespace dp {

namespace internal {
    template <std::size_t Dim, typename SShape, std::size_t... I>
    inline constexpr auto add_dim_shape_impl(std::index_sequence<I...>)
        -> StaticShape<(I == Dim ? 1 : SShape::to_shape()[I < Dim ? I : I - 1])...>;
    template <std::size_t Dim, typename SShape>
    using add_dim_shape = decltype(add_dim_shape_impl<Dim, SShape>(std::make_index_sequence<SShape::Rank + 1>{}));
}

class AddDimensionTag;
template <typename ChildExpr, std::size_t Dim>
struct AddDimension : Tensor<AddDimension<ChildExpr, Dim>,
                          typename ChildExpr::ValueType,
                          ChildExpr::rank + 1> {
    using Tag = AddDimensionTag;
    using SShape = internal::add_dim_shape<Dim, typename ChildExpr::SShape>;
    std::tuple<ChildExpr> m_args;

    static constexpr std::size_t NewRank = ChildExpr::rank + 1;

    static_assert(Dim <= ChildExpr::rank, "dim must be smaller than or equal to rank");

    explicit AddDimension(ChildExpr const& expr)
        : m_args(~expr)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();

        auto shape = for_range<NewRank>([&](auto... i) {
            return Shape{ (i == Dim ? 1 : a.shape[i < Dim ? i : i - 1])... };
        });

        auto lambda = [a = a.lambda](Queue& q) {
            return [a = a(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh)](auto idx) {
                    return a(for_range<ChildExpr::rank>([&](auto... i) {
                        return Index{ idx[(i < Dim ? i : i + 1)]... };
                    }));
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("AddDimension_") + std::to_string(Dim);
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("AddDimension"), Dim, ChildExpr::hash());
    }
};

template <std::size_t Dim, typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto add_dim(
    Tensor<ChildExpr, ValueType, Rank> const& x)
{
    return AddDimension<ChildExpr, Dim>(~x);
}
}

#endif // ADDDIMEXPR_HPP
