#ifndef SUMEXPR_HPP
#define SUMEXPR_HPP

#include "elemwiseexpr.hpp"
#include "indexexpr.hpp"
#include "reshapeexpr.hpp"
#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

namespace internal {
    template <std::size_t Dim, typename SShape, std::size_t... I>
    inline constexpr auto sum_nkd_shape_impl(std::index_sequence<I...>)
        -> StaticShape<(SShape::to_shape()[I < Dim ? I : I + 1])...>;
    template <std::size_t Dim, typename SShape>
    using sum_nkd_shape = decltype(sum_nkd_shape_impl<Dim, SShape>(std::make_index_sequence<SShape::Rank - 1>{}));

    template <std::size_t Dim, typename SShape, std::size_t... I>
    inline constexpr auto sum_kd_shape_impl(std::index_sequence<I...>)
        -> StaticShape<(I == Dim ? 1 : SShape::to_shape()[I])...>;
    template <std::size_t Dim, typename SShape>
    using sum_kd_shape = decltype(sum_kd_shape_impl<Dim, SShape>(std::make_index_sequence<SShape::Rank>{}));

    template <bool KeepDim, std::size_t AlongDim, typename SShape>
    using sum_shape = std::conditional_t<KeepDim, sum_kd_shape<AlongDim, SShape>, sum_nkd_shape<AlongDim, SShape>>;

    template <typename Operator, std::size_t AlongDim, typename Lambda>
    struct ReduceLambdaKD {
        Lambda a;
        std::size_t dim;
        auto operator()(Queue& q) const
        {
            return [a = a(q), dim = dim](cl::sycl::handler& cgh) {
                return [a = a(cgh), dim](auto idx) {
                    idx[AlongDim] = 0;
                    auto ret = a(idx);
                    for (std::size_t i = 1; i < dim; ++i) {
                        idx[AlongDim] = i;
                        ret = Operator{}(ret, a(idx));
                    }
                    return ret;
                };
            };
        }
    };

    template <typename Operator, std::size_t AlongDim, typename Lambda>
    struct ReduceLambdaNKD {
        Lambda a;
        std::size_t dim;
        auto operator()(Queue& q) const
        {
            return [a = a(q), dim = dim](cl::sycl::handler& cgh) {
                return [a = a(cgh), dim](auto idx) {
                    constexpr auto Rank = decltype(idx)::rank + 1;
                    Index<Rank> child_idx = for_range<Rank>([&](auto... i) {
                        return Index{ (i == AlongDim ? 0 : idx[i < AlongDim ? i : i - 1])... };
                    });
                    auto ret = a(child_idx);
                    for (std::size_t i = 1; i < dim; ++i) {
                        child_idx[AlongDim] = i;
                        ret = Operator{}(ret, a(child_idx));
                    }
                    return ret;
                };
            };
        }
    };
}

class ReduceExprTag;
template <typename Operator_, typename ChildExpr, std::size_t AlongDim_, bool KeepDim_ = false>
struct ReduceExpr : Tensor<ReduceExpr<Operator_, ChildExpr, AlongDim_, KeepDim_>,
                        typename ChildExpr::ValueType,
                        (KeepDim_ ? ChildExpr::rank : ChildExpr::rank - 1)> {
    using SShape = internal::sum_shape<KeepDim_, AlongDim_, typename ChildExpr::SShape>;
    using Tag = ReduceExprTag;
    using Operator = Operator_;
    static constexpr auto AlongDim = AlongDim_;
    static constexpr auto KeepDim = KeepDim_;
    static constexpr bool do_caching = true;

    std::tuple<ChildExpr> m_args;

    static constexpr std::size_t Rank = ChildExpr::rank;
    static_assert(AlongDim < Rank, "AlongDim must be smaller than Rank");

    explicit ReduceExpr(ChildExpr const& child)
        : m_args(~child)
    {
    }

    auto build() const
    {
        if constexpr (KeepDim) {
            auto a = this->arg0().get();

            Shape<Rank> new_shape = for_range<Rank>([&](auto... i) {
                return Shape{ (i == AlongDim ? 1 : a.shape[i])... };
            });

            internal::ReduceLambdaKD<Operator, AlongDim, decltype(a.lambda)> lambda{
                a.lambda, a.shape[AlongDim]
            };

            return make_pair(new_shape, lambda);

        } else {
            auto a = this->arg0().get();

            Shape<Rank - 1> new_shape = for_range<Rank - 1>([&](auto... i) {
                return Shape{ a.shape[i < AlongDim ? i : i + 1]... };
            });

            internal::ReduceLambdaNKD<Operator, AlongDim, decltype(a.lambda)> lambda{
                a.lambda, a.shape[AlongDim]
            };

            return make_pair(new_shape, lambda);
        }
    }

    std::string name() const
    {
        return "ReduceExpr";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ReduceExpr"),
            Operator::hash(), ChildExpr::hash(),
            AlongDim, KeepDim);
    }
};

template <typename Operator, std::size_t AlongDim, std::size_t... Rest,
    typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto reduce(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    if constexpr (sizeof...(Rest) == 0) {
        return ReduceExpr<Operator, ChildExpr, AlongDim>(~child);
    } else {
        return reduce<Operator, (Rest - (Rest > AlongDim))...>(
            reduce<Operator, AlongDim>(child));
    }
}

template <typename Operator, std::size_t AlongDim, std::size_t... Rest,
    typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto reduce_keep_dim(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    if constexpr (sizeof...(Rest) == 0) {
        return ReduceExpr<Operator, ChildExpr, AlongDim, true>(~child);
    } else {
        return reduce_keep_dim<Operator, Rest...>(
            reduce_keep_dim<Operator, AlongDim>(child));
    }
}

template <typename Operator, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto reduce_all(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce<Operator, 0>(reshape<UnknownDim>(child));
}

// Applications
template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto sum(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce<internal::functor_plus, AlongDims...>(child);
}

template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto sum_keep_dim(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_keep_dim<internal::functor_plus, AlongDims...>(child);
}

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto sum_all(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_all<internal::functor_plus>(child);
}

template <std::size_t AlongDim, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto mean(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return sum<AlongDim>(child)
        / cast<ValueType>(to_tensor(nth_dim<AlongDim>(shape_of(~child))));
}

template <std::size_t AlongDim, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto mean_keep_dim(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return sum_keep_dim<AlongDim>(~child)
        / cast<ValueType>(to_tensor(nth_dim<AlongDim>(shape_of(~child))));
}

template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto max(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce<internal::functor_fmax, AlongDims...>(child);
}

template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto max_keep_dim(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_keep_dim<internal::functor_fmax, AlongDims...>(child);
}

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto max_all(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_all<internal::functor_fmax>(child);
}

template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto min(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce<internal::functor_fmin, AlongDims...>(child);
}

template <std::size_t... AlongDims, typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto min_keep_dim(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_keep_dim<internal::functor_fmin, AlongDims...>(child);
}

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto min_all(Tensor<ChildExpr, ValueType, Rank> const& child)
{
    return reduce_all<internal::functor_fmin>(child);
}

template <std::size_t I = 0, std::size_t Rank,
    typename ChildExpr, typename ValueType,
    typename LikeExpr, typename LikeValueType>
inline auto sum_like(
    Tensor<ChildExpr, ValueType, Rank> const& child,
    Tensor<LikeExpr, LikeValueType, Rank> const& like)
{
    if constexpr (I == Rank) {
        return ~child;
    } else if constexpr (ChildExpr::SShape::to_shape()[I] != LikeExpr::SShape::to_shape()[I]) {
        static_assert(LikeExpr::SShape::to_shape()[I] == 1);
        return sum_like<I + 1>(sum_keep_dim<I>(child), like);
    } else {
        return sum_like<I + 1>(child, like);
    }
}
}

#endif // SUMEXPR_HPP
