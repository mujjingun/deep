#ifndef MULTMATRIXEXPR_HPP
#define MULTMATRIXEXPR_HPP

#include "tensorexpr.hpp"

namespace dp {

class MultMatrixMatrixTag;
template <typename LhsExpr, typename RhsExpr>
struct MultMatrixMatrix : Tensor<MultMatrixMatrix<LhsExpr, RhsExpr>,
                              typename LhsExpr::ValueType, 2> {
    using Tag = MultMatrixMatrixTag;
    static constexpr bool do_caching = true;

    std::tuple<LhsExpr, RhsExpr> m_args;

    using ValueType = typename LhsExpr::ValueType;

    explicit MultMatrixMatrix(
        LhsExpr const& lhs,
        RhsExpr const& rhs)
        : m_args(~lhs, ~rhs)
    {
    }

    auto build() const
    {
        auto [a_shape, a] = this->arg0().get();
        auto [b_shape, b] = this->arg1().get();

        D_ASSERT(a_shape[1] == b_shape[0], "Matrix inner dims must be the same");

        std::size_t n = a_shape[1];
        auto shape = Shape(a_shape[0], b_shape[1]);

        auto lambda = [n, a = a, b = b](Queue& q) {
            return [n, a = a(q), b = b(q)](cl::sycl::handler& cgh) {
                return [n, a = a(cgh), b = b(cgh)](auto idx) {
                    ValueType ret = ValueType(0);
                    for (std::size_t i = 0; i < n; ++i) {
                        ret += a(Index(idx[0], i)) * b(Index(i, idx[1]));
                    }
                    return ret;
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return std::string("MultMatrixMatrix");
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("MultMatrixMatrix"), LhsExpr::hash(), RhsExpr::hash());
    }
};

template <typename ValueType, typename LhsExpr, typename RhsExpr>
inline auto operator*(
    Tensor<LhsExpr, ValueType, 2> const& lhs,
    Tensor<RhsExpr, ValueType, 2> const& rhs)
{
    return MultMatrixMatrix<LhsExpr, RhsExpr>(~lhs, ~rhs);
}
}

#endif // MULTMATRIXEXPR_HPP
