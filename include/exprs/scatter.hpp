#ifndef SCATTER_HPP
#define SCATTER_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

namespace internal {
    template <std::int64_t I>
    class ScatterExprInitializeBuf;

    template <std::int64_t I>
    class ScatterExprPopulateBuf;
}

class ScatterExprTag;
template <typename Indices, typename Updates, typename ShapeType>
struct ScatterExpr : Tensor<ScatterExpr<Indices, Updates, ShapeType>,
                         typename Updates::ValueType, 2> {
    using Tag = ScatterExprTag;
    using ValueType = typename Updates::ValueType;
    using ThisType = ScatterExpr<Indices, Updates, ShapeType>;
    using SShape = typename ShapeType::SShape;

    static_assert(Indices::rank == 1);
    static_assert(Updates::rank == 2);
    static_assert(ShapeType::rank == 2);

    std::tuple<Updates> m_args;
    Indices m_indices;
    ShapeType m_shape;

    explicit ScatterExpr(
        Indices const& indices,
        Updates const& updates,
        ShapeType const& shape)
        : m_args(updates)
        , m_indices(indices)
        , m_shape(shape)
    {
    }

    auto build() const
    {
        auto indices = m_indices.get();
        auto updates = this->arg0().get();
        auto shape = m_shape.get();

        D_ASSERT(indices.shape[0] == updates.shape[0], "indices.shape[0] != updates.shape[0]");
        D_ASSERT(updates.shape[1] == shape[1], "updates.shape[1] != shape[1]");

        auto lambda = [indices = indices.lambda,
                          indices_shape = indices.shape,
                          updates = updates.lambda,
                          shape,
                          state = this->m_state](Queue& q) {
            using BufType = Buffer<ValueType, 2>;

            if (!state->data.has_value()) {

                // create buf
                state->data = BufType(shape);
                auto& buf = *std::any_cast<BufType>(&state->data);

                // initialize buf
                q.submit([&](cl::sycl::handler& cgh) {
                    auto wbuf = buf.get_write_access(cgh);
                    cgh.parallel_for<internal::ScatterExprInitializeBuf<static_cast<std::int64_t>(hash())>>(
                        to_range(shape),
                        [wbuf](ClIdType<2> id) {
                            wbuf.by_id(id) = ValueType(0);
                        });
                });

                // populate buf
                q.submit([&, indices = indices(q),
                             updates = updates(q)](cl::sycl::handler& cgh) {
                    auto wbuf = buf.get_write_access(cgh);
                    cgh.parallel_for<internal::ScatterExprPopulateBuf<static_cast<std::int64_t>(hash())>>(
                        to_range(indices_shape),
                        [wbuf, n = shape[1],
                            indices = indices(cgh),
                            updates = updates(cgh)](ClIdType<1> id) {
                            Index<2> idx;
                            idx[0] = indices(Index{ id[0] });
                            for (std::size_t i = 0; i < n; ++i) {
                                idx[1] = i;
                                wbuf[idx] = updates(Index{ id[0], i });
                            }
                        });
                });
            }

            auto& buf = *std::any_cast<BufType>(&state->data);
            return [&buf](cl::sycl::handler& cgh) {
                auto rbuf = buf.get_read_access(cgh);
                return [rbuf](auto idx) {
                    return rbuf[idx];
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "ScatterExpr";
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: Add shapetype
        return hash_combine(hash_str("ScatterExpr"), Indices::hash(), Updates::hash());
    }
};

template <typename Indices, typename IndexValueType,
    typename Updates, typename ValueType,
    typename ShapeType>
inline auto scatter(
    Tensor<Indices, IndexValueType, 1> const& indices,
    Tensor<Updates, ValueType, 2> const& updates,
    ShapeExpr<ShapeType, 2> const& shape)
{
    return ScatterExpr<Indices, Updates, ShapeType>(
        ~indices, ~updates, ~shape);
}

template <typename Indices, typename IndexValueType,
    typename Updates, typename ValueType>
inline auto scatter(
    Tensor<Indices, IndexValueType, 1> const& indices,
    Tensor<Updates, ValueType, 2> const& updates,
    Shape<2> shape)
{
    return scatter(indices, updates, lazy_shape(shape));
}
}

#endif // SCATTER_HPP
