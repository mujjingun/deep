#ifndef RESHAPEEXPR_HPP
#define RESHAPEEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

class ReshapeTag;
template <typename ChildExpr, typename ShapeType>
struct Reshape : Tensor<Reshape<ChildExpr, ShapeType>,
                     typename ChildExpr::ValueType,
                     ShapeType::rank> {
    using SShape = typename ShapeType::SShape;
    using Tag = ReshapeTag;
    std::tuple<ChildExpr> m_args;
    ShapeType m_shape;

    static constexpr std::size_t Rank = ShapeType::rank;

    explicit Reshape(
        ChildExpr const& expr,
        ShapeType const& shape)
        : m_args(~expr)
        , m_shape(~shape)
    {
    }

    auto build() const
    {
        auto [shape, a] = this->arg0().get();

        Shape<Rank> new_shape = m_shape.get();
        std::size_t size = shape.size();

        std::size_t n_unknowns = std::count(new_shape.begin(), new_shape.end(), UnknownDim);
        D_ASSERT(n_unknowns <= 1, "Number of unknown dimensions must be 0 or 1");

        if (n_unknowns == 1) {
            std::size_t remaining_size = 1;
            for (std::size_t i = 0; i < Rank; ++i) {
                if (new_shape[i] != UnknownDim) {
                    remaining_size *= new_shape[i];
                }
            }
            D_ASSERT(size % remaining_size == 0, "Cannot reshape to this size");
            for (std::size_t i = 0; i < Rank; ++i) {
                if (new_shape[i] == UnknownDim) {
                    new_shape[i] = size / remaining_size;
                }
            }
        }

        D_ASSERT(size == new_shape.size(), "Original size != New size");

        auto lambda = [a = a, shape = shape, new_shape](Queue& q) {
            return [a = a(q), shape, new_shape](cl::sycl::handler& cgh) {
                return [a = a(cgh), shape, new_shape](auto idx) {
                    auto fidx = flatten_index(idx, new_shape);
                    auto cidx = unflatten_index(fidx, shape);
                    return a(cidx);
                };
            };
        };

        return make_pair(new_shape, lambda);
    }

    std::string name() const
    {
        return std::string("Reshape");
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add ShapeType
        return hash_combine(hash_str("Reshape"), ChildExpr::hash());
    }
};

template <typename ValueType, std::size_t Rank, std::size_t NewRank,
    typename ChildExpr>
inline auto reshape(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    Shape<NewRank> shape)
{
    auto lshape = lazy_shape(shape);
    return Reshape<ChildExpr, decltype(lshape)>(
        ~expr, ~lshape);
}

template <typename ValueType, std::size_t Rank, typename ChildExpr,
    typename ShapeType, std::size_t NewRank>
inline auto reshape(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    ShapeExpr<ShapeType, NewRank> const& shape)
{
    return Reshape<ChildExpr, ShapeType>(~expr, ~shape);
}

template <std::size_t... Dims,
    typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto reshape(
    Tensor<ChildExpr, ValueType, Rank> const& expr)
{
    auto lshape = lazy_shape<Dims...>();
    return Reshape<ChildExpr, decltype(lshape)>(
        ~expr, ~lshape);
}

template <typename ValueType, std::size_t ChildRank, typename ChildExpr,
    typename LikeValueType, std::size_t NewRank, typename LikeExpr>
inline auto reshape_like(
    Tensor<ChildExpr, ValueType, ChildRank> const& expr,
    Tensor<LikeExpr, LikeValueType, NewRank> const& like)
{
    auto lshape = shape_of(like);
    return Reshape<ChildExpr, decltype(lshape)>(
        ~expr, ~lshape);
}
}

#endif // RESHAPEEXPR_HPP
