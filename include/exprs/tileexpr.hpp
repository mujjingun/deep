#ifndef TILEEXPR_HPP
#define TILEEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

class TileTensorTag;
template <typename ChildExpr, std::size_t Dim, typename DimType>
struct TileTensor : Tensor<TileTensor<ChildExpr, Dim, DimType>,
                        typename ChildExpr::ValueType, ChildExpr::rank> {
    using Tag = TileTensorTag;
    std::tuple<ChildExpr> m_args;
    DimType m_dim;

    explicit TileTensor(ChildExpr const& expr, DimType const& dim)
        : m_args(~expr)
        , m_dim(~dim)
    {
    }

    auto build() const
    {
        auto [shape, a] = this->arg0().get();

        D_ASSERT(shape[Dim] == 1, "The tiled dimension must be 1");
        shape[Dim] = m_dim.get();

        auto lambda = [a = a](Queue& q) {
            return [a = a(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh)](auto idx) {
                    idx[Dim] = 0;
                    return a(idx);
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "TileTensor";
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add DimType
        return hash_combine(hash_str("TileTensor"),
            ChildExpr::hash(), Dim);
    }
};

template <std::size_t Dim, typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto tile(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    std::size_t dim)
{
    auto ldim = lazy_dim(dim);
    return TileTensor<ChildExpr, Dim, decltype(ldim)>(
        ~expr, ~ldim);
}

template <std::size_t Dim, typename ValueType, std::size_t Rank,
    typename ChildExpr, typename LikeExpr>
inline auto tile_like(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    Tensor<LikeExpr, ValueType, Rank> const& like)
{
    auto ldim = nth_dim<Dim>(shape_of(like));
    return TileTensor<ChildExpr, Dim, decltype(ldim)>(
        ~expr, ~ldim);
}
}

#endif // TILEEXPR_HPP
