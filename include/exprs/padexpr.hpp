#ifndef PADEXPR_HPP
#define PADEXPR_HPP

#include "shapeexpr.hpp"
#include "tensorexpr.hpp"

namespace dp {

template <typename ChildExpr, typename BeforeShape, typename AfterShape>
struct PadTensor : Tensor<PadTensor<ChildExpr, BeforeShape, AfterShape>,
                       typename ChildExpr::ValueType,
                       ChildExpr::rank> {
    using ValueType = typename ChildExpr::ValueType;
    static constexpr std::size_t Rank = ChildExpr::rank;

    std::tuple<ChildExpr> m_args;
    BeforeShape m_before;
    AfterShape m_after;

    explicit PadTensor(
        ChildExpr const& expr,
        BeforeShape before,
        AfterShape after)
        : m_args(~expr)
        , m_before(~before)
        , m_after(~after)
    {
    }

    auto build() const
    {
        auto a = this->arg0().get();

        auto before = m_before.get();
        auto after = m_after.get();

        Shape<Rank> new_shape = for_range<Rank>([&](auto... i) {
            return Shape{ (before[i] + a.shape[i] + after[i])... };
        });

        auto lambda = [before, shape = a.shape, a = a.lambda](Queue& q) {
            return [before, shape, a = a(q)](cl::sycl::handler& cgh) {
                return [before, shape, a = a(cgh)](auto idx) {
                    if (for_range<Rank>([&](auto... i) {
                            return (... || (idx[i] < before[i])) || (... || (idx[i] >= before[i] + shape[i]));
                        })) {
                        return ValueType(0);
                    }
                    return a(for_range<Rank>([&](auto... i) {
                        return Index{ (idx[i] - before[i])... };
                    }));
                };
            };
        };

        return make_pair(new_shape, lambda);
    }

    std::string name() const
    {
        return std::string("PadTensor");
    }

    constexpr static std::uint64_t hash()
    {
        // TODO: add beforeshape and aftershape
        return hash_combine(hash_str("PadTensor"), ChildExpr::hash());
    }
};

template <typename ValueType, std::size_t Rank, typename ChildExpr>
inline auto pad(
    Tensor<ChildExpr, ValueType, Rank> const& expr,
    Shape<Rank> before, Shape<Rank> after)
{
    auto lbefore = lazy_shape(before);
    auto lafter = lazy_shape(after);
    return PadTensor<ChildExpr, decltype(lbefore), decltype(lafter)>(
        ~expr, ~lbefore, ~lafter);
}
}

#endif // PADEXPR_HPP
