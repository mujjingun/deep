#ifndef TENSOREXPR_HPP
#define TENSOREXPR_HPP

#include "exprbase.hpp"

namespace dp {

// A pair of (shape, lambda)
template <std::size_t Rank, typename Lambda>
struct Pair {
    Shape<Rank> shape;
    Lambda lambda;
};

template <std::size_t Rank, typename Lambda>
inline auto make_pair(Shape<Rank> shape, Lambda lambda)
{
    return Pair<Rank, Lambda>{ shape, lambda };
}

class InvalidTag;

namespace internal {
    template <std::int64_t I>
    class Tag0;
    template <std::int64_t I>
    class Tag1;
}

template <typename Child, typename ValueType_, std::size_t Rank>
struct Tensor : ExprBase<Tensor<Child, ValueType_, Rank>> {
    using ValueType = ValueType_;
    using TensorType = Tensor<Child, ValueType, Rank>;
    using IdType = ClIdType<Rank>;
    using SShape = make_unknown_shape<Rank>;
    using Tag = InvalidTag;
    static constexpr auto sshape = SShape::to_shape();

    static constexpr bool do_caching = false;

    static constexpr std::size_t rank = Rank;
    static constexpr std::size_t num_args = std::tuple_size_v<decltype(Child::m_args)>;

    Tensor() noexcept
    {
        static_assert(is_static_shape<typename Child::SShape>,
            "SShape needs to be an instance of dp::StaticShape");
        static_assert(Child::SShape::Rank == Rank,
            "SShape's rank must be the same as the Tensor's rank");
    }

    template <std::size_t N>
    constexpr auto& arg()
    {
        return std::get<N>((~(*this)).m_args);
    }

    template <std::size_t N>
    constexpr const auto& arg() const
    {
        return std::get<N>((~(*this)).m_args);
    }

    constexpr Child const& operator~() const
    {
        return static_cast<Child const&>(*this);
    }

    constexpr Child& operator~()
    {
        return static_cast<Child&>(*this);
    }

    void run_impl(Queue& q, Buffer<ValueType, Rank>& result)
    {
        using namespace cl::sycl;

        auto [shape, lambda] = get();

        D_ASSERT(shape == result.shape(), "Result buffer's shape must match with output");

        auto command = lambda(q);

        q.submit([&](handler& cgh) {
            auto eval = command(cgh);

            auto R = result.get_write_access(cgh);

            cgh.parallel_for<internal::Tag0<static_cast<std::int64_t>(Child::hash())>>(
                to_range(result.shape()),
                [R, eval](IdType id) {
                    Index<Rank> idx = R.to_index(id);
                    R.by_id(id) = eval(idx);
                });
        });
    }

    Buffer<ValueType, Rank> run_impl(Queue& q)
    {
        auto shape = get().shape;
        Buffer<ValueType, Rank> buf(shape);
        run_impl(q, buf);
        return buf;
    }

    auto get() const
    {
        using namespace cl::sycl;

        auto [shape, a] = (~*this).build();

        static_assert(std::is_same_v<decltype(shape), Shape<Rank>>);

        D_ASSERT(Child::SShape::to_shape().is_match(shape),
            "Static & actual shapes do not match");

        if constexpr (Child::do_caching) {

            auto state = this->m_state;

            if (!state->data.has_value()) {
                state->data = Buffer<ValueType, Rank>(shape);
            }
            auto& buf = *std::any_cast<Buffer<ValueType, Rank>>(&state->data);

            auto lambda = [state, &buf, a = a, shape = shape](Queue& q) {
                if (state->eval_cnt == 0) {
                    q.submit([&, a = a(q)](handler& cgh) {
                        auto wbuf = buf.get_write_access(cgh);
                        cgh.parallel_for<internal::Tag1<static_cast<std::int64_t>(Child::hash())>>(
                            to_range(shape),
                            [wbuf, a = a(cgh)](IdType id) {
                                wbuf.by_id(id) = a(wbuf.to_index(id));
                            });
                    });
                }

                state->eval_cnt++;

                return [&buf](handler& cgh) {
                    auto rbuf = buf.get_read_access(cgh);
                    return [rbuf](auto idx) {
                        return rbuf[idx];
                    };
                };
            };

            return make_pair(shape, lambda);

        } else {
            return make_pair(shape, a);
        }
    }

    template <typename... IndexTypes>
    inline auto index(IndexTypes... indices) const;
};

template <typename ValueType_, std::size_t Rank, typename SShape_>
class ErasedTensor {
public:
    using ValueType = ValueType_;
    using SShape = SShape_;
    constexpr static auto rank = Rank;

private:
    struct Concept {
        virtual Shape<Rank> get_shape() const = 0;
        virtual ~Concept() = default;
    };

    template <typename ExprType>
    struct Model : Concept {
        ExprType m_expr;

        explicit Model(ExprType const& expr)
            : m_expr(expr)
        {
        }

        Shape<Rank> get_shape() const override
        {
            return m_expr.get().shape;
        }
    };

    std::shared_ptr<Concept const> m_model;

public:
    template <typename ExprType>
    explicit ErasedTensor(Tensor<ExprType, ValueType, Rank> const& expr)
        : m_model(new Model<ExprType>{ ~expr })
    {
    }

    Shape<Rank> get_shape() const
    {
        return m_model->get_shape();
    }
};

template <typename Child, typename ValueType, std::size_t Rank>
inline auto erased_tensor(Tensor<Child, ValueType, Rank> const& expr)
{
    return ErasedTensor<ValueType, Rank, typename Child::SShape>(expr);
}
}

#endif // TENSOREXPR_HPP
