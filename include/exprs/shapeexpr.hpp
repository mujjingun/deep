#ifndef SHAPEEXPR_HPP
#define SHAPEEXPR_HPP

#include "shapes.hpp"
#include "tensorexpr.hpp"

namespace dp {

template <typename Child, std::size_t SDim_>
struct DimExpr : ExprBase<DimExpr<Child, SDim_>> {
    static constexpr auto SDim = SDim_;
    static constexpr std::size_t num_args = 0;

    constexpr Child const& operator~() const
    {
        return static_cast<Child const&>(*this);
    }

    constexpr Child& operator~()
    {
        return static_cast<Child&>(*this);
    }

    std::size_t get() const
    {
        return (~*this).build();
    }

    std::size_t run_impl(Queue&)
    {
        return get();
    }
};

template <std::size_t SDim_>
class ErasedDimExpr : public DimExpr<ErasedDimExpr<SDim_>, SDim_> {
public:
    constexpr static auto SDim = SDim_;

private:
    struct Concept {
        virtual std::size_t build() const = 0;
        virtual ~Concept() = default;
    };

    template <typename ExprType>
    struct Model : Concept {
        ExprType m_expr;

        explicit Model(ExprType const& expr)
            : m_expr(expr)
        {
        }

        std::size_t build() const override
        {
            return m_expr.build();
        }
    };

    std::shared_ptr<Concept const> m_model;

public:
    template <typename Child>
    explicit ErasedDimExpr(DimExpr<Child, SDim> const& expr)
        : m_model(new Model<Child>{ ~expr })
    {
    }

    std::size_t build() const
    {
        return m_model->build();
    }
};

template <typename Child, std::size_t SDim>
inline auto erased_dim_expr(DimExpr<Child, SDim> const& expr)
{
    return ErasedDimExpr<SDim>(expr);
}

template <std::size_t SDim = UnknownDim>
struct FixedDimExpr : DimExpr<FixedDimExpr<SDim>, SDim> {
    std::size_t m_dim;

    explicit FixedDimExpr(std::size_t dim)
        : m_dim(dim)
    {
    }

    std::size_t build() const
    {
        return m_dim;
    }
};

inline auto lazy_dim(std::size_t dim)
{
    return erased_dim_expr(FixedDimExpr<>(dim));
}

template <std::size_t SDim>
inline auto lazy_dim()
{
    return erased_dim_expr(FixedDimExpr<SDim>(SDim));
}

template <typename Operator, typename LhsChild, typename RhsChild>
struct DimBinaryExpr
    : DimExpr<DimBinaryExpr<Operator, LhsChild, RhsChild>,
          LhsChild::SDim != UnknownDim && RhsChild::SDim != UnknownDim
              ? Operator{}(LhsChild::SDim, RhsChild::SDim)
              : UnknownDim> {
    LhsChild lhs;
    RhsChild rhs;

    explicit DimBinaryExpr(LhsChild const& lhs, RhsChild const& rhs)
        : lhs(lhs)
        , rhs(rhs)
    {
    }

    std::size_t build() const
    {
        return Operator{}(lhs.get(), rhs.get());
    }
};

template <typename Operator,
    typename LhsChild, std::size_t LhsSDim,
    typename RhsChild, std::size_t RhsSDim>
inline auto binary_dim_op(
    DimExpr<LhsChild, LhsSDim> const& lhs,
    DimExpr<RhsChild, RhsSDim> const& rhs)
{
    return erased_dim_expr(
        DimBinaryExpr<Operator, LhsChild, RhsChild>(~lhs, ~rhs));
}

template <
    typename LhsChild, std::size_t LhsSDim,
    typename RhsChild, std::size_t RhsSDim>
inline auto operator+(
    DimExpr<LhsChild, LhsSDim> const& lhs,
    DimExpr<RhsChild, RhsSDim> const& rhs)
{
    return erased_dim_expr(binary_dim_op<std::plus<>>(lhs, rhs));
}

template <
    typename LhsChild, std::size_t LhsSDim,
    typename RhsChild, std::size_t RhsSDim>
inline auto operator-(
    DimExpr<LhsChild, LhsSDim> const& lhs,
    DimExpr<RhsChild, RhsSDim> const& rhs)
{
    return erased_dim_expr(binary_dim_op<std::minus<>>(lhs, rhs));
}

template <
    typename LhsChild, std::size_t LhsSDim,
    typename RhsChild, std::size_t RhsSDim>
inline auto operator*(
    DimExpr<LhsChild, LhsSDim> const& lhs,
    DimExpr<RhsChild, RhsSDim> const& rhs)
{
    return erased_dim_expr(binary_dim_op<std::multiplies<>>(lhs, rhs));
}

template <
    typename LhsChild, std::size_t LhsSDim,
    typename RhsChild, std::size_t RhsSDim>
inline auto operator/(
    DimExpr<LhsChild, LhsSDim> const& lhs,
    DimExpr<RhsChild, RhsSDim> const& rhs)
{
    return erased_dim_expr(binary_dim_op<std::divides<>>(lhs, rhs));
}

template <typename Child, std::size_t Rank>
struct ShapeExpr : ExprBase<ShapeExpr<Child, Rank>> {
    static constexpr std::size_t rank = Rank;
    static constexpr std::size_t num_args = 0;

    ShapeExpr() noexcept
    {
        static_assert(is_static_shape<typename Child::SShape>,
            "SShape needs to be an instance of dp::StaticShape");
        static_assert(Child::SShape::Rank == Rank,
            "SShape's rank must be the same as the Tensor's rank");
    }

    constexpr Child const& operator~() const
    {
        return static_cast<Child const&>(*this);
    }

    constexpr Child& operator~()
    {
        return static_cast<Child&>(*this);
    }

    Shape<Rank> get() const
    {
        return (~*this).build();
    }

    Shape<Rank> run_impl(Queue&)
    {
        return get();
    }
};

template <typename SShape_>
class ErasedShapeExpr : public ShapeExpr<ErasedShapeExpr<SShape_>, SShape_::Rank> {
public:
    using SShape = SShape_;
    static constexpr std::size_t rank = SShape::Rank;

private:
    struct Concept {
        virtual Shape<rank> build() const = 0;
        virtual ~Concept() = default;
    };

    template <typename ExprType>
    struct Model : Concept {
        ExprType m_expr;

        explicit Model(ExprType const& expr)
            : m_expr(expr)
        {
        }

        Shape<rank> build() const override
        {
            return m_expr.build();
        }
    };

    std::shared_ptr<Concept const> m_model;

public:
    template <typename Child>
    explicit ErasedShapeExpr(ShapeExpr<Child, rank> const& expr)
        : m_model(new Model<Child>{ ~expr })
    {
    }

    Shape<rank> build() const
    {
        return m_model->build();
    }
};

template <typename Child, std::size_t Rank>
inline auto erased_shape_expr(ShapeExpr<Child, Rank> const& expr)
{
    return ErasedShapeExpr<typename Child::SShape>(expr);
}

template <typename ShapeType, std::size_t Dim>
struct DimFromShapeExpr : DimExpr<DimFromShapeExpr<ShapeType, Dim>,
                              ShapeType::SShape::to_shape()[Dim]> {
    ShapeType m_shape;

    explicit DimFromShapeExpr(ShapeType const& shape)
        : m_shape(shape)
    {
    }

    std::size_t build() const
    {
        return m_shape.get()[Dim];
    }
};

template <std::size_t Dim, typename ShapeType, std::size_t Rank>
inline auto nth_dim(ShapeExpr<ShapeType, Rank> const& shape)
{
    return erased_dim_expr(DimFromShapeExpr<ShapeType, Dim>(~shape));
}

template <typename ErasedType>
struct ShapeFromTensor : ShapeExpr<ShapeFromTensor<ErasedType>,
                             ErasedType::rank> {
    using SShape = typename ErasedType::SShape;
    ErasedType m_expr;

    explicit ShapeFromTensor(ErasedType const& expr)
        : m_expr(expr)
    {
    }

    Shape<ErasedType::rank> build() const
    {
        return m_expr.get_shape();
    }
};

template <typename TensorExpr, typename ValueType, std::size_t Rank>
inline auto shape_of(Tensor<TensorExpr, ValueType, Rank> const& expr)
{
    auto erased = erased_tensor(expr);
    return erased_shape_expr(ShapeFromTensor<decltype(erased)>(erased));
}

template <typename SShape_, typename... DimTypes>
struct FixedShapeExpr : ShapeExpr<FixedShapeExpr<SShape_, DimTypes...>,
                            sizeof...(DimTypes)> {
    using SShape = SShape_;
    static constexpr auto Rank = sizeof...(DimTypes);
    std::tuple<DimTypes...> m_dims;

    explicit FixedShapeExpr(DimTypes const&... dims)
        : m_dims(~dims...)
    {
    }

    Shape<Rank> build() const
    {
        auto shape = std::apply([](auto const&... args) {
            return Shape{ args.get()... };
        },
            m_dims);

        D_ASSERT(SShape::to_shape().is_match(shape), "Static & actual shapes do not match ");
        return shape;
    }
};

template <std::size_t Rank>
inline auto lazy_shape(Shape<Rank> shape)
{
    auto result = for_range<Rank>([&](auto... i) {
        return FixedShapeExpr<make_unknown_shape<Rank>,
            dummy_type<decltype(i), FixedDimExpr<>>...>(
            FixedDimExpr<>(shape[i])...);
    });
    return erased_shape_expr(result);
}

template <std::size_t... Dims>
inline auto lazy_shape()
{
    auto result = FixedShapeExpr<StaticShape<Dims...>,
        dummy_type<decltype(Dims), FixedDimExpr<>>...>(
        FixedDimExpr<>(Dims)...);
    return erased_shape_expr(result);
}

template <typename... DimTypes, std::size_t... SDims>
inline auto lazy_shape(DimExpr<DimTypes, SDims> const&... dims)
{
    auto result = FixedShapeExpr<StaticShape<SDims...>, DimTypes...>(~dims...);
    return erased_shape_expr(result);
}

template <typename ChildExpr>
struct ShapeTensor : Tensor<ShapeTensor<ChildExpr>, std::size_t, 1> {
    using SShape = StaticShape<ChildExpr::rank>;
    std::tuple<> m_args;

    ChildExpr m_shape;

    explicit ShapeTensor(ChildExpr const& shape)
        : m_shape(~shape)
    {
    }

    auto build() const
    {
        auto shape = m_shape.get();

        auto lambda = [shape](Queue&) {
            return [shape](cl::sycl::handler&) {
                return [shape](auto idx) {
                    return shape[idx[0]];
                };
            };
        };

        return make_pair(Shape{ ChildExpr::rank }, lambda);
    }

    std::string name() const
    {
        return "ShapeTensor";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("ShapeTensor"), ChildExpr::rank);
    }
};

template <typename ChildExpr, std::size_t Rank>
inline auto to_tensor(ShapeExpr<ChildExpr, Rank> const& shape)
{
    return ShapeTensor<ChildExpr>(~shape);
}

template <typename ChildExpr>
struct DimTensor : Tensor<DimTensor<ChildExpr>, std::size_t, 0> {
    std::tuple<> m_args;

    ChildExpr m_dim;

    explicit DimTensor(ChildExpr const& dim)
        : m_dim(~dim)
    {
    }

    auto build() const
    {
        auto dim = m_dim.get();

        auto lambda = [dim](Queue&) {
            return [dim](cl::sycl::handler&) {
                return [dim](auto) {
                    return dim;
                };
            };
        };

        return make_pair(Shape{}, lambda);
    }

    std::string name() const
    {
        return "DimTensor";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("DimTensor"));
    }
};

template <typename Child, std::size_t SDim>
inline auto to_tensor(DimExpr<Child, SDim> const& dim)
{
    return DimTensor<Child>(~dim);
}
}

#endif // SHAPEEXPR_HPP
