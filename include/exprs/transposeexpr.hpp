#ifndef TRANSPOSEEXPR_HPP
#define TRANSPOSEEXPR_HPP

#include "tensorexpr.hpp"

namespace dp {

class TransposeTensorTag;
template <std::size_t Dim1_, std::size_t Dim2_, typename ChildExpr>
struct TransposeTensor : Tensor<TransposeTensor<Dim1_, Dim2_, ChildExpr>,
                             typename ChildExpr::ValueType, ChildExpr::rank> {
    using Tag = TransposeTensorTag;
    constexpr static auto Dim1 = Dim1_, Dim2 = Dim2_;

    std::tuple<ChildExpr> m_args;

    static_assert(Dim1 <= ChildExpr::rank, "dim1 must be smaller than or equal to rank");
    static_assert(Dim2 <= ChildExpr::rank, "dim1 must be smaller than or equal to rank");

    explicit TransposeTensor(ChildExpr const& expr)
        : m_args(~expr)
    {
    }

    auto build() const
    {
        auto [child_shape, a] = this->arg0().get();

        auto shape = child_shape;
        std::swap(shape[Dim1], shape[Dim2]);

        auto lambda = [a = a](Queue& q) {
            return [a = a(q)](cl::sycl::handler& cgh) {
                return [a = a(cgh)](auto idx) {
                    std::swap(idx[Dim1], idx[Dim2]);
                    return a(idx);
                };
            };
        };

        return make_pair(shape, lambda);
    }

    std::string name() const
    {
        return "TransposeTensor";
    }

    constexpr static std::uint64_t hash()
    {
        return hash_combine(hash_str("TransposeTensor"), Dim1, Dim2, ChildExpr::hash());
    }
};

template <std::size_t Dim1, std::size_t Dim2, typename ValueType, std::size_t Rank,
    typename ChildExpr>
inline auto transpose(Tensor<ChildExpr, ValueType, Rank> const& expr)
{
    return TransposeTensor<Dim1, Dim2, ChildExpr>(~expr);
}
}

#endif // TRANSPOSEEXPR_HPP
