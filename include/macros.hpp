#ifndef DASSERT_HPP
#define DASSERT_HPP

#ifdef __SYCL_DEVICE_ONLY__
#define D_ASSERT(cond, msg)
#else
#define D_ASSERT(cond, msg) assert((cond) && (msg))
#endif

#endif // DASSERT_HPP
