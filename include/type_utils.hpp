#ifndef TUPLE_UTILS_HPP
#define TUPLE_UTILS_HPP

#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

namespace dp {
namespace internal {
    template <typename F, typename Tuple1, typename Tuple2, std::size_t... Is>
    inline constexpr decltype(auto) apply_impl(F&& f,
        Tuple1&& tuple1, Tuple2&& tuple2,
        std::index_sequence<Is...>)
    {
        return f(std::forward_as_tuple(
            std::get<Is>(std::forward<Tuple1>(tuple1)),
            std::get<Is>(std::forward<Tuple2>(tuple2)))...);
    }
}

template <typename F, typename Tuple1, typename Tuple2>
inline constexpr decltype(auto) apply(F&& f, Tuple1&& tuple1, Tuple2&& tuple2)
{
    constexpr auto size = std::tuple_size_v<std::decay_t<Tuple1>>;
    static_assert(size == std::tuple_size_v<std::decay_t<Tuple2>>);
    return internal::apply_impl(std::forward<F>(f),
        std::forward<Tuple1>(tuple1), std::forward<Tuple2>(tuple2),
        std::make_index_sequence<size>{});
}

namespace internal {
    template <typename F, size_t... Is>
    static inline constexpr decltype(auto) for_range_impl(F&& f, std::index_sequence<Is...>)
    {
        return f(Is...);
    }

    template <typename F, size_t... Is>
    static inline constexpr decltype(auto) for_range_static_impl(F&& f, std::index_sequence<Is...>)
    {
        return f(std::integral_constant<std::size_t, Is>{}...);
    }
}

template <std::size_t N, typename F>
static inline constexpr decltype(auto) for_range(F&& f)
{
    return internal::for_range_impl(std::forward<F>(f),
        std::make_index_sequence<N>{});
}

template <std::size_t N, typename F>
static inline constexpr decltype(auto) for_range_static(F&& f)
{
    return internal::for_range_static_impl(std::forward<F>(f),
        std::make_index_sequence<N>{});
}

// Hold<1, 2, 3>.apply(f)
// -> Hold<2, 3>.apply(f, 1)
// -> Hold<3>.apply(f, 1, 2)
// -> Hold<>.apply(f, 1, 2, 3)

template <class... Ts>
class Hold;

template <class T, class... Rest>
class Hold<T, Rest...> {
    T m_v;
    Hold<Rest...> m_rest;

public:
    explicit Hold(T v, Rest... a)
        : m_v(v)
        , m_rest(a...)

    {
    }

    template <class F, class... Args>
    auto apply(F f, Args... a) const
    {
        return m_rest.apply(f, a..., m_v);
    }
};

template <>
class Hold<> {
public:
    Hold()
    {
    }

    template <class F, class... Args>
    auto apply(F f, Args... a) const
    {
        return f(a...);
    }
};

template <typename... Ts>
Hold(Ts...)->Hold<Ts...>;

struct filter_out_t {
};

template <typename F>
constexpr inline decltype(auto) filter(F&& f)
{
    return f();
}

template <typename F, typename Arg0, typename... Args>
constexpr inline decltype(auto) filter(F&& f, Arg0&& arg0, Args&&... args)
{
    if constexpr (std::is_same_v<std::decay_t<Arg0>, filter_out_t>) {
        return filter(
            [&](auto&&... a) {
                return f(std::forward<decltype(a)>(a)...);
            },
            std::forward<Args>(args)...);
    } else {
        return filter(
            [&](auto&&... a) {
                return f(std::forward<Arg0>(arg0), std::forward<decltype(a)>(a)...);
            },
            std::forward<Args>(args)...);
    }
}

struct nonesuch {
    ~nonesuch() = delete;
    nonesuch(nonesuch const&) = delete;
    void operator=(nonesuch const&) = delete;
};

namespace internal {
    template <class Default, class AlwaysVoid,
        template <class...> class Op, class... Args>
    struct detector {
        using value_t = std::false_type;
        using type = Default;
    };

    template <class Default, template <class...> class Op, class... Args>
    struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
        using value_t = std::true_type;
        using type = Op<Args...>;
    };

} // namespace detail

template <template <class...> class Op, class... Args>
using is_detected = typename internal::detector<nonesuch, void, Op, Args...>::value_t;

template <typename, typename T>
using dummy_type = T;

template <typename... Ts>
class display;

template <auto... Vs>
class display_val;

} // namespace dp
#endif // TUPLE_UTILS_HPP
