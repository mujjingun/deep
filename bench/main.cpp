#include <benchmark/benchmark.h>
#include <deep.hpp>
#include <random>

static void syclMatMul(benchmark::State& state)
{
    using namespace dp;

    Queue q{ cl::sycl::intel_selector{} };

    std::size_t N = state.range(0);

    std::vector<double> a(N * N);
    std::vector<double> b(N * N);

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    for (auto _ : state) {
        auto A = var(Shape(N, N), a.begin());
        auto B = var(Shape(N, N), b.begin());

        auto mul = A * B;
        auto buf = mul.run(q);

        auto C = buf.get_read_access();

        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                benchmark::DoNotOptimize(C[Index{ i, j }]);
            }
        }
    }
}
BENCHMARK(syclMatMul)->Range(8, 1 << 10);

static void cpuMatMul(benchmark::State& state)
{
    std::size_t N = state.range(0);

    std::vector<double> a(N * N);
    std::vector<double> b(N * N);

    std::vector<double> c(N * N);

    std::mt19937 gen(std::random_device{}());
    std::uniform_real_distribution<double> dis(-10.0, 10.0);
    std::generate(a.begin(), a.end(), [&] { return dis(gen); });
    std::generate(b.begin(), b.end(), [&] { return dis(gen); });

    for (auto _ : state) {
        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t k = 0; k < N; ++k) {
                for (std::size_t j = 0; j < N; ++j) {
                    c[i * N + j] += a[i * N + k] * b[k * N + j];
                }
            }
        }

        for (std::size_t i = 0; i < N; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                benchmark::DoNotOptimize(c[i * N + j]);
            }
        }
    }
}
BENCHMARK(cpuMatMul)->Range(8, 1 << 10);

BENCHMARK_MAIN();
