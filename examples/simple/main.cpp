#include <deep.hpp>

#include <iostream>

int main()
{
    dp::Queue q{ cl::sycl::intel_selector{} };

    auto input = dp::var(dp::Shape{ 10 }, { 0., 1., 2., 3., 4., 5., 6., 7., 8., 9. });
    auto expr = dp::elem_mul(input, input);

    auto result = expr.run(q);
    auto acc = result.get_read_access();

    for (std::size_t i = 0; i < acc.shape()[0]; ++i) {
        std::cout << acc[dp::Index{i}] << ", ";
    }
    std::cout << "\n";
}
