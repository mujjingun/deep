#include <cmath>
#include <deep.hpp>
#include <gradients.hpp>
#include <random>

#include "readdata.hpp"

dp::Queue q{ cl::sycl::intel_selector{} };

std::mt19937 gen{ std::random_device{}() };

dp::VariableTensor<float, 4> xavier_initializer(dp::Shape<4> shape)
{
    std::vector<float> init(shape.size());
    std::size_t n_in = shape[0] * shape[1] * shape[2];
    std::size_t n_out = shape[3];
    std::normal_distribution<float> dis(0, std::sqrt(2.0f / (n_in + n_out)));

    std::generate(init.begin(), init.end(), [&] { return dis(gen); });

    return dp::var(shape, init.begin());
}

dp::VariableTensor<float, 2> xavier_initializer(dp::Shape<2> shape)
{
    std::vector<float> init(shape.size());
    std::size_t n_in = shape[0];
    std::size_t n_out = shape[1];
    std::normal_distribution<float> dis(0, std::sqrt(2.0f / (n_in + n_out)));

    std::generate(init.begin(), init.end(), [&] { return dis(gen); });

    return dp::var(shape, init.begin());
}

template <std::size_t Rank>
dp::VariableTensor<float, Rank> constant_initializer(dp::Shape<Rank> shape, float value)
{
    std::vector<float> init(shape.size());
    std::fill(init.begin(), init.end(), value);

    return dp::var(shape, init.begin());
}

struct CNN {
    dp::VariableTensor<float, 4> w0;
    dp::VariableTensor<float, 1> b0;
    dp::VariableTensor<float, 4> w1;
    dp::VariableTensor<float, 1> b1;

    dp::VariableTensor<float, 2> wout;
    dp::VariableTensor<float, 1> bout;

    float learning_rate = 0.01;

    CNN()
        : w0(xavier_initializer(dp::Shape{ 5, 5, 1, 6 }))
        , b0(constant_initializer(dp::Shape{ 6 }, 0.01))
        , w1(xavier_initializer(dp::Shape{ 5, 5, 6, 12 }))
        , b1(constant_initializer(dp::Shape{ 12 }, 0.01))
        , wout(xavier_initializer(dp::Shape{ 192, 10 }))
        , bout(constant_initializer(dp::Shape{ 10 }, 0.01))
    {
    }

    float train(std::vector<float> const& images, std::vector<int> const& labels)
    {
        std::size_t batch_size = labels.size();

        auto input = dp::var(dp::Shape{ batch_size, 28, 28, 1 }, images.begin());

        auto W0 = dp::set_id<class W0>(w0);
        auto B0 = dp::set_id<class B0>(b0);
        auto W1 = dp::set_id<class W1>(w1);
        auto B1 = dp::set_id<class B1>(b1);
        auto Wout = dp::set_id<class Wout>(wout);
        auto Bout = dp::set_id<class Bout>(bout);
        auto weights = dp::make_tuple(W0, B0, W1, B1, Wout, Bout);

        // b x 12 x 12 x 6
        auto conv0 = dp::max_pooling(
            dp::elem_fmax(
                dp::conv2d(input, W0) + B0, dp::var(0.0f)),
            dp::Shape{ 2, 2 });

        // b x 4 x 4 x 12
        auto conv1 = dp::max_pooling(
            dp::elem_fmax(
                dp::conv2d(conv0, W1) + B1, dp::var(0.0f)),
            dp::Shape{ 2, 2 });

        // b x 192
        auto reshape = dp::reshape(conv1, dp::Shape{ batch_size, dp::UnknownDim });

        // b x 10
        auto out = reshape * Wout + Bout;

        // softmax
        auto exp = dp::elem_exp(out);
        auto softmax = dp::elem_div(exp, dp::sum_keep_dim<1>(exp));

        // loss calculation
        auto indices = dp::var(dp::Shape{ batch_size }, labels.begin());
        auto cross_entropy = -dp::elem_log(
            softmax.index(dp::arange<float>(batch_size), indices));
        auto loss = dp::mean<0>(cross_entropy);

        // update weights
        auto grad = dp::grad(loss, weights);
        auto updates = dp::apply([&](auto const&... pairs) {
            return dp::make_tuple(
                std::get<0>(pairs) + dp::var(-learning_rate) * std::get<1>(pairs)...);
        },
            weights, grad);

        auto do_update = dp::apply([&](auto const&... pairs) {
            return dp::make_tuple(
                dp::update(std::get<0>(pairs), std::get<1>(pairs))...);
        },
            weights, updates);

        auto [_, l] = dp::make_tuple(do_update, loss).run(q);

        return l.get_read_access()[dp::Index{}];
    }
};

int main()
{
    // training data
    auto train = readMnist("data/train-images.idx3-ubyte", "data/train-labels.idx1-ubyte", 0);

    // validation data
    auto valid = readMnist("data/t10k-images.idx3-ubyte", "data/t10k-labels.idx1-ubyte", 0);

    std::size_t batch_size = 64;

    CNN cnn;

    std::size_t n = 0;
    while (true) {
        std::vector<float> images;
        std::vector<int> labels;

        std::cout << "n = " << n % train.size() << "\n";

        for (std::size_t i = 0; i < batch_size; ++i) {
            auto& example = train[(n + i) % train.size()];
            std::copy(example.image.begin(), example.image.end(), std::back_inserter(images));
            labels.push_back(example.label);
        }

        n += batch_size;

        std::cout << cnn.train(images, labels) << "\n";
    }
}
