#ifndef MNIST_READDATA_HPP
#define MNIST_READDATA_HPP

#include <vector>

struct MNIST {
    std::vector<float> image;
    int label;
};

std::vector<MNIST> readMnist(const char* image_file, const char* label_file, int padding);

#endif
