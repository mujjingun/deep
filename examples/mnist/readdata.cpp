#include "readdata.hpp"
#include <cassert>
#include <cstdio>
#include <cstdlib>

static void changeEndian(void* vp, size_t sz)
{
    char* begin = (char*)vp;
    char* end = (char*)vp + sz - 1;
    while (begin < end) {
        *begin ^= *end;
        *end ^= *begin;
        *begin ^= *end;
        begin++;
        end--;
    }
}

std::vector<MNIST> readMnist(const char* image_file, const char* label_file, int padding)
{
    int magic_number, num_of_images, rows, cols;

    // read image file
    FILE* fp_image = fopen(image_file, "rb");
    assert(fp_image != NULL);
    fread(&magic_number, sizeof(int), 1, fp_image);
    changeEndian(&magic_number, sizeof(int));
    assert(magic_number == 2051);
    fread(&num_of_images, sizeof(int), 1, fp_image);
    changeEndian(&num_of_images, sizeof(int));
    fread(&rows, sizeof(int), 1, fp_image);
    changeEndian(&rows, sizeof(int));
    fread(&cols, sizeof(int), 1, fp_image);
    changeEndian(&cols, sizeof(int));

    // read label file
    FILE* fp_label = fopen(label_file, "rb");
    int num_of_items;
    assert(fp_label != NULL);
    fread(&magic_number, sizeof(int), 1, fp_label);
    changeEndian(&magic_number, sizeof(int));
    assert(magic_number == 2049);
    fread(&num_of_items, sizeof(int), 1, fp_label);
    changeEndian(&num_of_items, sizeof(int));
    assert(num_of_items == num_of_images);

    std::vector<MNIST> ret(num_of_images);

    std::vector<unsigned char> p(rows * cols);
    for (int i = 0; i < num_of_images; i++) {
        ret[i].image = std::vector<float>((rows + padding * 2) * (cols + padding * 2));
        unsigned char label;
        fread(&label, sizeof(unsigned char), 1, fp_label);
        ret[i].label = label;
        fread(p.data(), 1, rows * cols, fp_image);
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < cols; x++) {
                ret[i].image[(y + padding) * (cols + padding * 2) + (x + padding)] = p[y * cols + x] / 255.0F;
            }
        }
    }

    return ret;
}
